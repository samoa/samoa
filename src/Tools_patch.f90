#include "Compilation_control.f90"

module Tools_patch

    use SFC_edge_traversal

    public patch_geometry
    
    ! ************************************************************************************************************
    ! patch_geometry
    !
    !   Describes the geometry of a patch.
    !   
    !   Created by Chaulio Ferreira. Contact: chaulio.ferreira@tum.de or chauliof@gmail.com
    !
    !   In this implementation, a patch is obtained by recursive subdivision of the cell using the newest-vertex
    !   bisection method. This is basically the same as the adaptive sam(oa)² mesh, except that here all cells are
    !   refined up to a user-specified depth d>=0, resulting in 2^d triangular cells with the same area. 
    !
    !   Note that, in order to avoid hanging nodes in the sam(oa)² mesh, we restrict d to even values. Also, d=0
    !   is allowed and means that the patch contains a single cell.
    !
    !   To facilitate compiler optimizations (especially auto-vectorization), d>=0 is defined at compilation
    !   time using a #define macro (_PATCH_DEPTH). This is currently handled by SCons, which adds a -D flag when 
    !   compiling. These other values are computed from _PATCH_DEPTH using macros (see Compilation_control.f90):
    !
    !     _PATCH_NUM_CELLS = (2**_PATCH_DEPTH)
    !     _PATCH_NUM_EDGES = (3*(2**(_PATCH_DEPTH-1) + 2**(_PATCH_DEPTH/2 - 1)))
    !     _PATCH_BOUNDARY_SIZE = (2**(_PATCH_DEPTH/2))
    !
    !
    !   How to use this module in a nutshell (including a few important geometry considerations):
    !    
    !   1) First of all, patch_geometry%init() must be called (currently called in main.f90).
    !
    !   2) Cell numbering: cells are numbered from 1 to _PATCH_NUM_CELLS. Their order is defined as follows:
    !      We start with a single cell with id=1. Then, for every recursive subdivision every cell i will be split
    !      into two new cells: the left one with id=2i-1 and a right one with id=2i. For all means, we define 
    !      "left" and "right" considering an isosceles right triangle with its hypotenuse parallel to the x axis 
    !      and its right angle pointing upwards. Consider this definition every time you read "left" or "right".
    !
    !   3) Coordinates: you can use the function patch_geometry%get_vertices(cell_id, plotter_type) to get the 
    !      x-y coordinates of the three vertices (in a [0,1]x[0,1] domain, where each patch leg lies on an axis).
    !      The vertices are ordered like this: 
    !         1 = vertex with right angle;
    !         2 = vertex left to the hypotenuse
    !         3 = vertex right to the hypotenuse
    !      The plotter_type is usually found in element%cell%geometry%i_plotter_type and is necessary to cope with
    !      the geometric transformations performed by samoa when computing real world coordinates. The advantage 
    !      is that now the coordinates returned by this function can be directly used to get the real world coordinates,
    !      like this:
    !      real_world = samoa_barycentric_to_world_point(element%transform_data, coordinates) * cfg%scaling + cfg%offset
    !
    !   4) Edges: edges are numbered from 1 to _PATCH_NUM_EDGES (including ghost edges -> see (6) and (7))
    !      Information about each edge can be obtained with patch_geometry%get_edge(edge_id, ...).
    !      This information includes the ids of the two cells adjacent to the edge, and whether they are inside the patch
    !      (cell_type=0) or in one of the three patch boundaries (cell_type=1, 2 or 3). Again, see (6) and (7) for more
    !      explanations about this.
    !
    !   5) Edge and cell orientations: these are integers in the range [0,7] that can be found in patch_geometry%edge_orientations
    !      and patch_geometry%cell_orientations. They are used to describe how this cell/edge is rotated.
    !      For cells, orientation=0 means that we have the base triangle described in (2).
    !      For edges, orientation=0 means that we have the hypotenuse of that triangle.
    !      If orientation != 0, then this edge/cell can be obtained by rotating the base triangle by (orientation*45) degrees 
    !      in the counterclockwise direction. Note: I don't think these need to be used outside this module!
    !
    !   6) Boundary cells: each of the three patch boundaries is adjacent to _PATCH_BOUNDARY_SIZE cells. To find out which
    !      are these cells, you need to use the patch_geometry%get_boundary_cell(boundary_id, cell_id_in_boundary),
    !      with boundary_id in [1,3] and cell_id_in_boundary in [1,_PATCH_BOUNDARY_SIZE]. 
    !      Here (and in all cases where patch boundary need to be numbered), 1=left leg, 2=hypotenuse and 3=right leg.
    !
    !   7) Ghost cells: Right outside the patch boundaries, we consider that 3*_PATCH_BOUNDARY_SIZE ghost cells exist,
    !      each one adjacent to one of the boundary cells. They are also numbered from 1 to _PATCH_BOUNDARY_SIZE for
    !      each of the three boundaries, such that the i-th boundary cell of the j-th boundary is adjacent to the 
    !      i-th ghost cell of the j-th boundary.
    !
    !   8) Adaptivity: when refining/coarsening patches, it is necessary to know the relationships between the cell ids
    !      in the original and fine/coarse patches. Because of the cell numbering we use (see (2)), this can be easily
    !      obtained, but I've created the functions patch_geometry%get_child_ids() and %get_parent_id() to make it
    !      even easier.
    !
    ! ************************************************************************************************************
    type t_patch_geometry
    
        ! =======================    
        ! Arrays describing cells:
        
        ! Coordinates of cells vertices, used e.g. grid initialization and for producing visual output.
        real (kind = GRID_SR), dimension(2,3,_PATCH_NUM_CELLS) :: coords
        
        ! Stores the three neighbors of each cell, in this order: "left" leg, hypotenuse, "right" leg
        integer, dimension(3,_PATCH_NUM_CELLS) :: neighbors
        
        ! Stores which cells are at each boundary (cells with ghost neighbors). They are divided into three groups:
        !   1 = boundary at the "left" leg
        !   2 = boundary at the hypotenuse
        !   3 = boundary at the "right" leg
        integer, dimension(_PATCH_BOUNDARY_SIZE,3) :: boundary_cells
        
        ! Orientation of each cell. The "base" orientation (0) is a triangle with its hypotenuse parallel to the
        !   x axis and the vertex with the right angle pointing upwards. These values indicate how this base 
        !   triangle should be rotated to match each cell. They are integers ranging from 0 to 7, indicating a 
        !   rotation of (orientation*45) degrees in the counterclockwise direction.
        integer, dimension(_PATCH_NUM_CELLS) :: cell_orientations
            
            
        ! =======================
        ! Arrays describing edges:
        
        ! This one means that triangles with ids edges(1,i) and edges(2,i) are neighbors, and edge number i is between them.
        integer, dimension(2,_PATCH_NUM_EDGES)  :: edges
        ! Describes edge orientation. This follows the same pattern of cell_orientations, but considering that the "base" edge (0)
        !   is the hypotenuse of the "base" cell described above (the hypotenuse is parallel to the x axis and the triangle points upwards)
        integer, dimension(_PATCH_NUM_EDGES)    :: edge_orientations
        ! Edge type: 1=left leg, 2=hypotenuse, 3=hypotenuse (in relation to the cell, not to the patch). Useful to compute the edge length.
        integer, dimension(_PATCH_NUM_EDGES)    :: edge_types
        ! A look-up table of edge normals depending on the edge orientation:
        real (kind=GRID_SR), dimension(2, 0:7)  :: table_edge_normals 
        
        
        contains
        
        ! methods used to get geometry data
        procedure, pass :: init => patch_geometry_init
        procedure, pass :: get_vertices => patch_geometry_get_vertices
        procedure, pass :: get_boundary_cell => patch_geometry_get_boundary_cell
        procedure, pass :: get_edge => patch_geometry_get_edge
        procedure, pass :: get_child_ids => patch_geometry_get_child_ids
        procedure, pass :: get_parent_id => patch_geometry_get_parent_id

    end type

    type(t_patch_geometry) :: patch_geometry

    contains 
    
    !*****************************************************************
    ! PUBLIC Procedures/functions for t_patch_geometry
    !*****************************************************************
    
    !> @brief Initializes geometry data for the patches. This needs to be called before anything else.
    !> @param geom Input Instance of t_patch_geometry
    subroutine patch_geometry_init(geom)
        class(t_patch_geometry), intent(inout) :: geom
        
        ! PARAMETERS:
        
        ! Depth of the patch.
        integer, parameter :: d = _PATCH_DEPTH
        ! Number of triangular cells in the patch, not considering ghost cells.
        ! A patch has 2^d internal cells plus 3 * 2^(d/2) ghost cells on its boundaries.
        integer, parameter :: n_cells = _PATCH_NUM_CELLS
        ! Total number of edges. 
        ! This is computed as 3*(2^d + 2^(d/2)/2) =  3*(2^(d-1) + 2^(d/2 - 1))
        integer, parameter :: n_edges = _PATCH_NUM_EDGES
        integer, parameter :: boundary_size = _PATCH_BOUNDARY_SIZE
        
        ! LOCAL VARIABLES:
        integer :: i
        integer :: counter_edges, counters_boundary_cells(3), boundary_id
        
        ! Create initial patch of depth = 0 (a single cell)
        call patch_geometry_get_base_patch(geom)
        
        ! Iteratively refine all cells in the patch until we reach the desired depth 
        ! (This fills arrays 'neighbors' and 'coords')
        do i=1, d
            call patch_geometry_refine_patch(geom, i-1)
        end do
        
        ! use the information we have in 'neighbors' and 'cell_orientations' to initialize 'edges', edge_orientations and 'boundary_cells'
        call patch_geometry_compute_edges(geom, n_cells, boundary_size)

    end subroutine
    
    !> @brief Returns the coordinates of the three vertices of a cell in the patch (in a [0,1]x[0,1] domain, where each patch leg lies on an axis).
    !> @param geom Input Instance of t_patch_geometry.
    !> @param cell_id Input Id of the cell (between 1 and _PATCH_NUM_CELLS = 2^_PATCH_DEPTH).
    !> @param plotter_type Input Plotter type of the patch (usually stored in element%cell%geometry%i_plotter_type).
    function patch_geometry_get_vertices(geom, cell_id, plotter_type) result(vertices)
        class(t_patch_geometry), intent(in) :: geom
        integer, intent(in) :: cell_id, plotter_type
        real (kind=GRID_SR), dimension(2,3) :: vertices
        
        ! local 
        real (kind=GRID_SR), dimension(2) :: tmp
        
        vertices(:,:) = geom%coords(:,:,cell_id)
        
        ! if plotter_type is negative, the cell is actually mirrored and it is still
        ! necessary to transform the coordinates like this:
        if (plotter_type < 0) then
            ! invert x and y axes
            vertices(:,1) = [vertices(2,1), vertices(1,1)]
            vertices(:,2) = [vertices(2,2), vertices(1,2)]
            vertices(:,3) = [vertices(2,3), vertices(1,3)]
            
            ! invert v2 and v3
            tmp = vertices(:,2)
            vertices(:,2) = vertices(:,3)
            vertices(:,3) = tmp
        end if
    end function
    
    !> @brief Returns information about a given edge.
    !> @param geom Input Instance of t_patch_geometry.
    !> @param edge_id Input Id of the edge (between 1 and _PATCH_NUM_EDGES).
    !> @param plotter_type Input Plotter type of the patch (usually stored in element%cell%geometry%i_plotter_type).
    !> @param edge Output Integer(2) containing the ids of the cells on each side of the edge. (See also cell_types)
    !> @param cell_types Output Integer(2) identifying where the data for each cell must come from. 0 = internal, 1 = left boundary, 2 = bottom (hypotenuse), 3 = right.
    !> @param edge_normal Output A real(2) vector normal to the edge pointing from edge(1) to edge(2).
    subroutine patch_geometry_get_edge(geom, edge_id, plotter_type, edge, cell_types, edge_normal) 
        class(t_patch_geometry), intent(in)             :: geom
        integer, intent(in)                             :: edge_id, plotter_type
        integer, dimension(2), intent(out)              :: edge, cell_types
        real(kind=GRID_SR), dimension(2), intent(out)   :: edge_normal
        
        ! local
        integer i;
        
        edge = geom%edges(:,edge_id)
        
        do i=1,2
            if (edge(i) <= _PATCH_NUM_CELLS) then
                cell_types(i) = 0
            else if (edge(i) <= _PATCH_NUM_CELLS + 1*_PATCH_BOUNDARY_SIZE) then
                cell_types(i) = 1
                edge(i) = edge(i) - _PATCH_NUM_CELLS
            else if (edge(i) <= _PATCH_NUM_CELLS + 2*_PATCH_BOUNDARY_SIZE) then
                cell_types(i) = 2
                edge(i) = edge(i) - _PATCH_NUM_CELLS - 1*_PATCH_BOUNDARY_SIZE
            else
                cell_types(i) = 3
                edge(i) = edge(i) - _PATCH_NUM_CELLS - 2*_PATCH_BOUNDARY_SIZE
            end if
        end do
        
        ! get edge_normal depending on plotter_type and geom%edge_orientations(edge_id)
        ! note: (abs(plotter_type) + 3) gives us the patch orientation according to the definitions we use here.
        edge_normal = geom%table_edge_normals(:, mod(abs(plotter_type) + 3 + geom%edge_orientations(edge_id) , 8) )
        
    end subroutine
    
    !> @brief Returns the id of a cell in the boundary of the patch.
    function patch_geometry_get_boundary_cell(geom, boundary_id, cell_id_in_boundary) result(id)
        class(t_patch_geometry), intent(in) :: geom
        integer, intent(in) :: boundary_id, cell_id_in_boundary
        integer :: id
        
        id = geom%boundary_cells(cell_id_in_boundary, boundary_id)
        
    end function
    
    !> @brief When refining a patch, this returns the ids of the "children" refined cells originated from parent_cell_id
    !> @param geom Input Instance of t_patch_geometry.
    !> @param parent_cell_id Input Id of the parent cell (between 1 and _PATCH_NUM_CELLS = 2^_PATCH_DEPTH).
    !> @param child_cell_ids Output Ids of the two children
    !> @param child_patch_id Output Used to tell to which refined patch these cells belong. 1=left patch, 2=right patch
    subroutine patch_geometry_get_child_ids(geom, parent_cell_id, child_cell_ids, child_patch_id)
        class(t_patch_geometry), intent(in) :: geom
        integer, intent(in)                 :: parent_cell_id
        integer, dimension(2), intent(out)  :: child_cell_ids
        integer, intent(out)                :: child_patch_id
        
        ! local 
        integer :: x
        
        ! Logic used here:
        !  --> Cells numbered from 1 to _PATCH_NUM_CELLS/2 are refined into two cells of the LEFT child patch (child_patch_id = 1)
        !      These cells have ids (2*parent_cell_id -1) and (2*parent_cell_id).
        !  --> Cells numbered from _PATCH_NUM_CELLS/2 + 1 to _PATCH_NUM_CELLS are refined into two cells of the RIGHT child patch. (child_patch_id = 2)
        !      These cells have ids (2*x -1) and (2*x), where x = parent_cell_id - _PATCH_NUM_CELLS/2.
        
        if (parent_cell_id <= _PATCH_NUM_CELLS / 2) then
            child_patch_id = 1
            child_cell_ids(1) = 2*parent_cell_id - 1
            child_cell_ids(2) = 2*parent_cell_id
        else
            child_patch_id = 2
            x = parent_cell_id - _PATCH_NUM_CELLS / 2
            child_cell_ids(1) = 2*x - 1
            child_cell_ids(2) = 2*x
        end if
    end subroutine
    
    !> @brief When coarsening a patch, this returns the id of the "parent" coarse cell originated from child_cell_id
    !> @param geom Input Instance of t_patch_geometry.
    !> @param child_cell_id Input Id of child the cell (between 1 and _PATCH_NUM_CELLS = 2^_PATCH_DEPTH).
    !> @param child_patch_id Input Ids of child patch. 1=left patch, 2=right patch
    !> @param parent_cell_id Output Id of the parent cell in the coarse patch.
    subroutine patch_geometry_get_parent_id(geom, child_cell_id, child_patch_id, parent_cell_id)
        class(t_patch_geometry), intent(in) :: geom
        integer, intent(in)                 :: child_cell_id
        integer, intent(in)                 :: child_patch_id
        integer, intent(out)                :: parent_cell_id
        
        ! local 
        integer :: x
        
        ! The logic used here is basically the inverse of the one used in patch_geometry_get_child_ids.
        
        if (child_patch_id == 1) then
            parent_cell_id = (child_cell_id + 1) / 2
        else
            parent_cell_id = (child_cell_id + 1) / 2 + _PATCH_NUM_CELLS / 2
        end if
    end subroutine
    
    
    !*****************************************************************
    ! PRIVATE Procedures/functions for t_patch_geometry
    !*****************************************************************
    
    ! Returns a patch with depth = 0
    ! Note: this is not a complete t_patch_geometry object, as not all members are initialized here.
    ! This subroutine is only used as one of the steps in patch_geometry_init(). 
    subroutine patch_geometry_get_base_patch(geom)
        CLASS(t_patch_geometry), INTENT(OUT) :: geom
        
        ! Init variables
        geom%coords(:,:,:) = 0
        geom%neighbors(:,:) = 0
        geom%cell_orientations(:) = 0
        
        ! this cell is neighbor to ghost cells at all three edges
        geom%neighbors(:,1) = [-1,-2,-3] 
        
        ! Coordinate system: (0,0) at the right angle; x axis on the left patch edge; y axis on the right patch edge.
        ! The right angle is the first vertex, and they are stored in counterclockwise order.
        geom%coords(:,1,1) = [0.0_GRID_SR, 0.0_GRID_SR]
        geom%coords(:,2,1) = [1.0_GRID_SR, 0.0_GRID_SR]
        geom%coords(:,3,1) = [0.0_GRID_SR, 1.0_GRID_SR]
        
        ! The first cell is the base cell (0)
        geom%cell_orientations(1) = 0
        
    end subroutine
    
    subroutine patch_geometry_refine_patch(geom, current_depth)
        class(t_patch_geometry), intent(inout) :: geom
        integer, intent(in) :: current_depth
        
        ! local variables
        type(t_patch_geometry) :: tmp_geom
        integer :: i, j
        integer :: n, new_cell
        integer :: a, b, c, new_a, new_b, new_c
        real(kind=GRID_SR), dimension(2) :: v1, v2, v3, m
        integer :: o ! orientation
        
        ! compute neighbors for refined patch
        do i=1, 2**current_depth
            ! Logic used here:
            !  Every n-th cell with neighbors (a, b, c) is split into two cells with ids (2n-1) and (2n), whose neighbors are:
            !  (2n-1) --> (2n, 2a, 2b)
            !  (2n)   --> (2b-1, 2c-1, 2n-1)
            
            ! Note that we still need to handle special cases for neighbors in the ghost layer. At this point, we only store whether 
            ! the ghost neighbor is adjacent to the "left" leg (-1), to the hypotenuse (-2) or to the "right" leg (-3). This is
            ! handled by the ternary operators in the code below.
            
            ! get data from old cell
            n = i
            a = geom%neighbors(1,i)
            b = geom%neighbors(2,i)
            c = geom%neighbors(3,i)

            ! Compute neighbors for new cell 2i-1
            new_cell = 2*n-1
            new_a = 2 * n
            new_b = merge(2*a, a, a>0) ! merge = ternary operator: (a>0 ? 2*a, a)
            new_c = merge(2*b, b, b>0)
            tmp_geom%neighbors(:,new_cell) = [new_a,new_b,new_c]
            
            ! Compute neighbors for new cell 2i
            new_cell = 2*n
            new_a = merge(2*b-1, b, b>0)
            new_b = merge(2*c-1, c, c>0)
            new_c = 2*n-1
            tmp_geom%neighbors(:,new_cell) = [new_a,new_b,new_c]
        end do
        
        ! compute coordinates of cell vertices for refined patch
        do i=1, 2**current_depth
            ! Logic used here:
            !  Every n-th cell has three vertices (v1, v2, v3).
            !  The vertices are sorted in counterclockwise order. More specifically:
            !   v1 -> vertex at the right edge (across the hypotenuse, between the two legs)
            !   v2 -> vertex between the "left" leg and the hypotenuse
            !   v3 -> vertex between the hypotenuse and the "right" leg
            !  This cell will be split into two cells with ids (2n-1) and (2n), whose vertices will be:
            !   (2n-1) --> (m, v1, v2)
            !   (2n)   --> (m, v3, v1), where m is the midpoint over the hypotenuse (midpoint between v2 and v3).
            !  Note that this definition also respects the ordering described above for the input cell.
            
            ! get data from old cell
            n = i
            v1 = geom%coords(:,1,i)
            v2 = geom%coords(:,2,i)
            v3 = geom%coords(:,3,i)
            
            ! compute midpoint over the hypotenuse
            m = 0.5 * (v2+v3)
            
            ! store vertices for new cell 2i-1
            new_cell = 2*i-1
            tmp_geom%coords(:,1,new_cell) = m
            tmp_geom%coords(:,2,new_cell) = v1
            tmp_geom%coords(:,3,new_cell) = v2
            
            ! store vertices for new cell 2i
            new_cell = 2*i
            tmp_geom%coords(:,1,new_cell) = m
            tmp_geom%coords(:,2,new_cell) = v3
            tmp_geom%coords(:,3,new_cell) = v1
        end do
        
        ! compute orientation of each cell in the refined patch
        do i=1, 2**current_depth
            ! Logic used here:
            !  Every n-th cell with orientation 'o' is split into two other cells.
            !  The first  one (2n-1) is rotated 225 degrees (5*45) in comparison to n.
            !  The second one (2n-1) is rotated 135 degrees (3*45) in comparison to n.
            ! We use use mod(new_o,8) to always have values between 0 and 7 (rotations between 0 and 315 degrees)
            
            ! get orientation from old cell
            o = geom%cell_orientations(i)

            ! set orientation of new cells
            tmp_geom%cell_orientations(2*i-1) = mod(o+5,8)
            tmp_geom%cell_orientations(2*i)   = mod(o+3,8)
       
        end do

        ! copy tmp variable to output
        call patch_geometry_copy(geom,tmp_geom) ! geom = tmp_geom
    
    end subroutine

    ! uses the information we have in 'neighbors' and 'cell_orientations' to initialize 'edges', edge_orientations and 'boundary_cells'.
    subroutine patch_geometry_compute_edges(geom, n_cells, boundary_size)
        class(t_patch_geometry), intent(inout) :: geom
        integer, intent(in) :: n_cells, boundary_size

        ! local vars
        integer :: i, j
        integer :: counter_edges, counters_boundary_cells(3), boundary_id
        real (kind=GRID_SR) :: sqrt_2_div_2


        counter_edges = 1 ! counter used to fill 'edges'
        counters_boundary_cells(:) = 1

        do i=1,n_cells ! for every internal cell
            do j=1,3 ! for all three neighbors
                if (i < geom%neighbors(j,i) .or. geom%neighbors(j,i) < 0) then ! avoid duplicated edges (a-b and b-a)
                    geom%edges(:,counter_edges) = [i, geom%neighbors(j,i)]
                    geom%edge_types(counter_edges) = j ! j=1 -> left leg, j=2 -> hypotenuse, j=3 -> right leg
                    
                    ! use correct numbering for boundary (ghost) edges and fill array 'boundary_cells'
                    if (geom%edges(2,counter_edges) < 0) then
                        boundary_id = - geom%edges(2,counter_edges) ! -1, -2 or -3 will become 1, 2 or 3
                        geom%boundary_cells(counters_boundary_cells(boundary_id), boundary_id) = i
                        
                        ! use a different id for each ghost cell
                        ! logic used here (consider n = _PATCH_NUM_CELLS and b = _PATCH_BOUNDARY_SIZE):
                        !  - cells from 1       to n        ---> internal cells;
                        !  - cells from n+1     to n+b      ---> ghost cells on the "left" boundary
                        !  - cells from n+b+1   to n+2b     ---> ghost cells on the "bottom" (hypotenuse) boundary
                        !  - cells from n+2b+1  to n+3b     ---> ghost cells on the "right" boundary
                        geom%edges(2,counter_edges) = n_cells + (boundary_id-1)*boundary_size + counters_boundary_cells(boundary_id)
                        
                        ! increment counter for boundary_cells(boundary_id)
                        counters_boundary_cells(boundary_id) = counters_boundary_cells(boundary_id) + 1
                    end if
                    
                    ! store edge orientation
                    if (j == 1) then
                        geom%edge_orientations(counter_edges) = mod(geom%cell_orientations(i) + 5, 8) ! this edge is rotated 5*45=225 degrees compared to the hypotenuse
                    else if (j == 2) then
                        geom%edge_orientations(counter_edges) = geom%cell_orientations(i) ! this edge is the hypotenuse
                    else
                        geom%edge_orientations(counter_edges) = mod(geom%cell_orientations(i) + 3, 8) ! this edge is rotated 3*45=135 degrees compared to the hypotenuse
                    end if
                    
                    ! increment counter for edges
                    counter_edges = counter_edges + 1 
                end if
            end do
        end do

        ! init look-up table containing edge normals
        sqrt_2_div_2 = sqrt(2.0) * 0.5_GRID_SR
        geom%table_edge_normals(:,0) = [ 0.0, -1.0]
        geom%table_edge_normals(:,1) = [ 1.0, -1.0] * sqrt_2_div_2
        geom%table_edge_normals(:,2) = [ 1.0,  0.0]
        geom%table_edge_normals(:,3) = [ 1.0,  1.0] * sqrt_2_div_2
        geom%table_edge_normals(:,4) = [ 0.0,  1.0]
        geom%table_edge_normals(:,5) = [-1.0,  1.0] * sqrt_2_div_2
        geom%table_edge_normals(:,6) = [-1.0,  0.0]
        geom%table_edge_normals(:,7) = [-1.0, -1.0] * sqrt_2_div_2
        
    end subroutine
    
    subroutine patch_geometry_copy(geom1, geom2)
        class(t_patch_geometry), intent(out) :: geom1
        class(t_patch_geometry), intent(in)  :: geom2
        
        ! copy all arrays from geom2 to geom1
        geom1%neighbors = geom2%neighbors
        geom1%coords = geom2%coords
        geom1%boundary_cells = geom2%boundary_cells
        geom1%cell_orientations = geom2%cell_orientations
        
        geom1%edges = geom2%edges
        geom1%edge_orientations = geom2%edge_orientations
        
    end subroutine
    
end module
