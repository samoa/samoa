! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE

#include "Compilation_control.f90"

    MODULE FVM_SWE2L

#if defined(_FVM_SWE2L)

        use Samoa
        use SWE2L_Scenario ! To simplify implementations of artificial scenarios, the module from 'src/SWE2L' is also used here
        use SWE2L_Riemann_solver_wrapper ! this is also used by SWE scenario
        
        contains
        
! ################################################################################################################
! # Below you will find a list of all operators that are MANDATORY for any scenario using this FVM code.
! # If any of these is missing, the code will likely not compile. If any of they are not correctly implemented, 
! # the simulation will crash or generate wrong results.
! # You will find more information about how to define each one of them in the comments above their code.
! #
! # List of required subroutines:
! #     - FVM_get_computational_domain
! #     - FVM_initialize_cell
! #     - FVM_timestep_compute_fluxes_single_edge OR FVM_timestep_compute_fluxes_multi_edge
! #     - FVM_timestep_update_cell
! #     - FVM_adapt_split_cell
! #     - FVM_adapt_merge_cells
! #     - FVM_output_pre_process_data 
! #     - FVM_output_labels
! # 
! # Note1: You do not need to implement both FVM_timestep_compute_fluxes_single_edge and
! # FVM_timestep_compute_fluxes_multi_edge. If you implement only the single_edge operator, you do not need to do 
! # anything. If you implement only the multi_edge operator, you will need to "#define _FVM_MULTI_EDGE".
! # If you implement both, you also don't need to do anything, and the usar will be able to switch between
! # both operators using the SCons parameter 'fvm_multi_edge'.
! #
! # Note2: There are more subroutines defined here, but they are not required and can be safely removed
! # (assuming that none of the required functions uses them)
! # 
! ################################################################################################################
        
        

! ################################################################################################################
! # General operators for the scenario:
! ################################################################################################################
       
       
        ! ************************************************************************************************************
        ! FVM_get_computational_domain:
        !
        !   Used to specify the computational domain. 
        !
        !   Parameters:
        !   (out)   scaling: the computational domain will have this size in both dimensions (scaling x scaling). [real]
        !   (out)   offset: xy-coordinates of the domain origin. [real(2)]
        !
        !   The resulting computational will be:
        !       [offset(1), offset(1) + scaling] x [offset(2), offset(2) + scaling]
        !
        !   Example: If we want the computational domain to be [-100,100] x [-100,100], we need to set:
        !       scaling = 200           ! total size of each dimension
        !       offset  = [-100, -100]  ! xy-coordinates of the origin
        ! ************************************************************************************************************
        subroutine FVM_get_computational_domain(scaling, offset)
            real (kind = GRID_SR),                  intent(out)    :: scaling
            real (kind = GRID_SR), dimension(2),    intent(out)    :: offset

#           if defined (_ASAGI)
                associate(afh_d => cfg%afh_displacement, afh_b => cfg%afh_bathymetry)
                    scaling = max(asagi_grid_max(afh_b, 0) - asagi_grid_min(afh_b, 0), asagi_grid_max(afh_b, 1) - asagi_grid_min(afh_b, 1))

                    offset = [0.5_GRID_SR * (asagi_grid_min(afh_d, 0) + asagi_grid_max(afh_d, 0)), 0.5_GRID_SR * (asagi_grid_min(afh_d, 1) + asagi_grid_max(afh_d, 1))] - 0.5_GRID_SR * cfg%scaling
                    offset = min(max(cfg%offset, [asagi_grid_min(afh_b, 0), asagi_grid_min(afh_b, 1)]), [asagi_grid_max(afh_b, 0), asagi_grid_max(afh_b, 1)] - cfg%scaling)

               end associate
            
#           else            
                ! Use routines from SWE2L_Scenario module here (from src/SWE2L)
                scaling = SWE2L_Scenario_get_scaling()
                offset  = SWE2L_Scenario_get_offset()
#           endif
        end subroutine

        
        
! ################################################################################################################
! # Operators required for FVM_initialize:
! ################################################################################################################
        
        
        ! ************************************************************************************************************
        ! FVM_initialize_cell:
        !
        !   Given the coordinates of the cell's vertices, this should return the Q and AUX data for that cell,
        !   as well as information about whether to further refine the cell and an estimate for the wave speed
        !   that should happen in this cell (this will be used to define the length of the first time step).
        !
        !   Parameters:
        !   (in)    vertices: 3 xy-coordinates, one for each cell's vertex. [real(2,3)]
        !   (out)   Q: Q quantities for this cell. [real(_FVM_Q_SIZE)]
        !   (out)   AUX: AUX quantities for this cell. [real(_FVM_AUX_SIZE)]
        !   (out)   estimated_wave_speed: an estimated value to be used for the first time step [real]
        !   (out)   refinement_flag: how this cell should be treated in the next time step. [integer]
        !
        !           Possible values for refinement_flag:
        !            1 -> refine it
        !            0 -> keep it
        !           -1 -> coarsen it
        !           (What will actually happen to this cell will also depend on its neighbor cells and patches)
        ! ************************************************************************************************************
        subroutine FVM_initialize_cell(vertices, Q, AUX, estimated_wave_speed, refinement_flag)
            real (kind = GRID_SR), dimension(2,3),              intent(in)      :: vertices
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE),      intent(out)     :: Q
            real (kind = GRID_SR), dimension(_FVM_AUX_SIZE),    intent(out)     :: AUX
            real (kind = GRID_SR),                              intent(out)     :: estimated_wave_speed
            integer (kind = BYTE),                              intent(out)     :: refinement_flag
            
            !local
            real (kind = GRID_SR) :: h1, hu1, hv1, h2, hu2, hv2, b
            real (kind = GRID_SR), dimension(3) :: barycenter
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE,3) :: Q_test
            integer :: i

            ! compute barycenter of cell, used to get the quantities in it
            barycenter(1:2) = (vertices(:,1) + vertices(:,2) + vertices(:,3)) / 3.0_GRID_SR

            ! The four functions used below are helper functions that are not mandatory for the FVM interface!
            Q = FVM_SWE2L_get_initial_Q(barycenter)
            AUX(1) = FVM_SWE2L_get_bathymetry(barycenter)

            ! extract the data to separate variables to make the remainder of this routine a lot easier to read
            call FVM_SWE2L_extract_Q_data(Q, h1, hu1, hv1, h2, hu2, hv2)
            call FVM_SWE2L_extract_AUX_data(AUX, b)

            ! estimative for wave speed:
            if (h1 - b > 0.0_GRID_SR) then
                estimated_wave_speed = sqrt(g * (h1 - b)) + sqrt((hu1 * hu1 + hv1 * hv1 / ((h1 - b) * (h1 - b))))
            else
                estimated_wave_speed = 0.0_GRID_SR
            end if
            
            ! refinement condition (extracted from SWE_initialize)
            refinement_flag = 0 ! init flag as 0

            !refine any slopes in the initial state
            do i = 1, 3
                Q_test(:,i) = FVM_SWE2L_get_initial_Q([vertices(1,i), vertices(2,i), 0.0_GRID_SR])
            end do

            if (maxval(Q_test(1,:)) > minval(Q_test(1,:))) then ! if max(h) > min(h)
                refinement_flag = 1
            end if
            
        end subroutine
        
        
        
! ################################################################################################################
! # Operators required for FVM_euler_timestep:
! ################################################################################################################
        
        
        ! ************************************************************************************************************
        ! FVM_timestep_compute_fluxes_single_edge:
        !
        !   NOTE: This operator is only used if the macro '_FVM_MULTI_EDGE' is NOT defined!
        !
        !   Given the Q and AUX quantities on each side of an edge, this functions needs to solve the respective Riemann 
        !   problem and return the "left"- and "right"-going fluxes at the edge, as well as the speed of the fastest wave 
        !   found in the Riemann solution. The fluxes will be used to compute the update for each cell, which will later  
        !   be used in FVM_timestep_update_cell().
        !
        !   Note1: The words "left" and "right" should not be taken literally here, because that is almost always not the case.
        !   In most cases, the edge is not even vertical. To obtain the actual geometry of the Riemann problem, please 
        !   consider the edge normal, which is given as an input. It always points from the "left" cell to the "right" cell.
        !
        !   Note2: The !$OMP directive in the beginning of the operator body is recommended and usually necessary 
        !   for successful vectorization with the Intel Compiler, if the option PATCH_VEC is set to "SIMD".
        !
        !   Parameters:
        !   (in)    normal: an xy-vector, normal to the cell edge (pointing from the "left" cell to the "right" cell). 
        !               This is typically used to transform vector unknowns to a system of coordinates relative to the 
        !               edge (e.g., hu and hv in SWE). [real(2)]
        !   (in)    qL: Q quantities for the cell to the "left" of the edge. [real(_FVM_Q_SIZE)]
        !   (in)    qR: Q quantities for the cell to the "right" of the edge. [real(_FVM_Q_SIZE)]
        !   (in)    auxL: AUX quantities for the cell to the "left" of the edge. [real(_FVM_AUX_SIZE)]
        !   (in)    auxR: AUX quantities for the cell to the "right" of the edge. [real(_FVM_AUX_SIZE)]
        !   (out)   fluxL: "left"-going "flux" (solution of Riemann problem). [real(_FVM_Q_SIZE)]
        !   (out)   fluxR: "right"-going "flux" (solution of Riemann problem). [real(_FVM_Q_SIZE)]
        !   (out)   maxWaveSpeed: speed of the fastest wave found in the Riemann solution. [real]
        !
        !
        ! ************************************************************************************************************
        subroutine FVM_timestep_compute_fluxes_single_edge(normal, qL, qR, auxL, auxR, fluxL, fluxR, maxWaveSpeed)
#           if defined(_PATCH_VEC_SIMD)
#               if defined(__MIC__) 
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_single_edge) simdlen(8) processor(mic)
#               elif defined(__AVX512F__)
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_single_edge) simdlen(8)
#               else
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_single_edge) simdlen(4) 
#               endif
#           endif
            real(kind = GRID_SR), dimension(2),             intent(in)      :: normal
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(inout)   :: qL, qR
            real(kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(inout)   :: auxL, auxR
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(inout)   :: fluxL, fluxR
            real(kind = GRID_SR),                           intent(out)     :: maxWaveSpeed

            ! local
            real(kind = GRID_SR)                :: transform_matrix(2, 2)
            real(kind = GRID_SR)                :: pL, pR ! pressure forcing, not considered here
            real(kind = GRID_SR)                :: waveSpeeds(6) ! output of Riemann solver: sw
            real(kind = GRID_SR)                :: fWaves(6,6) ! output of Riemann solver: fw
            integer                             :: equationNumber, waveNumber, i, j
            real(kind = GRID_SR)                :: hL, hR, huL, huR, hvL, hvR, bL, bR
            real(kind = GRID_SR)                :: hL2, hR2, huL2, huR2, hvL2, hvR2
            real(kind = GRID_SR)                :: upd_hL, upd_huL, upd_hvL, upd_hR, upd_huR, upd_hvR
            real(kind = GRID_SR)                :: upd_hL2, upd_huL2, upd_hvL2, upd_hR2, upd_huR2, upd_hvR2
            real(kind = GRID_SR)                :: maxWaveSpeedLocal
            
            ! Used to call the Riemann solver:
            real(kind=8), dimension(2) :: h_l, h_r, hu_l, hu_r, hv_l, hv_r
            real(kind=8):: b_l, b_r
            real(kind=8), dimension(2) :: h_hat_l, h_hat_r
            
            ! extract data from input arrays to local variables
            call FVM_SWE2L_extract_Q_data(qL, hL, huL, hvL, hL2, huL2, hvL2)
            call FVM_SWE2L_extract_AUX_data(auxL, bL)
            call FVM_SWE2L_extract_Q_data(qR, hR, huR, hvR, hR2, huR2, hvR2)
            call FVM_SWE2L_extract_AUX_data(auxR, bR)

            !DIR$ FORCEINLINE
            call SWE2L_compute_fluxes(normal(1), normal(2), hL, hR, huL, huR, hvL, hvR, bL, bR, &
                            hL2, hR2, huL2, huR2, hvL2, hvR2, upd_hL, upd_hR, upd_huL, upd_huR, upd_hvL, upd_hvR, &
                            upd_hL2, upd_hR2, upd_huL2, upd_huR2, upd_hvL2, upd_hvR2, maxWaveSpeedLocal)
                                        
            fluxL = [upd_hL, upd_huL, upd_hvL, upd_hL2, upd_huL2, upd_hvL2]
            fluxR = [upd_hR, upd_huR, upd_hvR, upd_hR2, upd_huR2, upd_hvR2]
            maxWaveSpeed = maxWaveSpeedLocal

        end subroutine
        
        
        ! ************************************************************************************************************
        ! FVM_timestep_compute_fluxes_multi_edge:
        !
        !   NOTE: This operator is only used if the macro '_FVM_MULTI_EDGE' IS defined!
        !
        !   Given the Q and AUX quantities on each side of an edge, this functions needs to solve the respective Riemann 
        !   problem and return the "left"- and "right"-going fluxes at the edge, as well as the speed of the fastest wave 
        !   found in the Riemann solution. The fluxes will be used to compute the update for each cell, which will later  
        !   be used in FVM_timestep_update_cell().
        !
        !   Note1: The words "left" and "right" should not be taken literally here, because that is almost always not the case.
        !   In most cases, the edge is not even vertical. To obtain the actual geometry of the Riemann problem, please 
        !   consider the edge normal, which is given as an input. It always points from the "left" cell to the "right" cell.
        !
        !   Note2: The !$OMP directive in the beginning of the operator body is recommended and usually necessary 
        !   for successful vectorization with the Intel Compiler, if the option PATCH_VEC is set to "SIMD".
        !
        !   Parameters:
        !   (in)    numProblems: number of problems in the arrays. While all arrays should have _FVM_CHUNK_SIZE
        !               dimension, sometimes they will not be completely filled with problems. This number will always be
        !               at least 1 and at most _FVM_CHUNK_SIZE.
        !   (in)    normal: an array of  xy-vectors, normal to the cell edges (pointing from the "left" cells to the "right" cells). 
        !               This is typically used to transform vector unknowns to a system of coordinates relative to the 
        !               edge (e.g., hu and hv in SWE). [real(_FVM_CHUNK_SIZE, 2)]
        !   (in)    qL: Q quantities for the cells to the "left" of the edges. [real(_FVM_CHUNK_SIZE, _FVM_Q_SIZE)]
        !   (in)    qR: Q quantities for the cells to the "right" of the edges. [real(_FVM_CHUNK_SIZE, _FVM_Q_SIZE)]
        !   (in)    auxL: AUX quantities for the cells to the "left" of the edges. [real(_FVM_CHUNK_SIZE, _FVM_AUX_SIZE)]
        !   (in)    auxR: AUX quantities for the cells to the "right" of the edges. [real(_FVM_CHUNK_SIZE, _FVM_AUX_SIZE)]
        !   (out)   fluxL: "left"-going "fluxes" (solutions of Riemann problems). [real(_FVM_CHUNK_SIZE, _FVM_Q_SIZE)]
        !   (out)   fluxR: "right"-going "fluxes" (solutions of Riemann problems). [real(_FVM_CHUNK_SIZE, _FVM_Q_SIZE)]
        !   (out)   maxWaveSpeed: speed of the fastest wave found in ALL Riemann solutions. [real]
        !
        !
        ! ************************************************************************************************************
        subroutine FVM_timestep_compute_fluxes_multi_edge(numProblems, normals, qL, qR, auxL, auxR, fluxL, fluxR, maxWaveSpeed)
#           if defined(_PATCH_VEC_SIMD)
#               if defined(__MIC__) 
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_multi_edge) simdlen(8) processor(mic)
#               elif defined(__AVX512F__)
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_multi_edge) simdlen(8)
#               else
                    !$OMP DECLARE SIMD(FVM_timestep_compute_fluxes_multi_edge) simdlen(4) 
#               endif
#           endif
            integer,                                                         intent(in)      :: numProblems
            real(kind = GRID_SR), dimension(_FVM_CHUNK_SIZE, 2),             intent(in)      :: normals
            real(kind = GRID_SR), dimension(_FVM_CHUNK_SIZE, _FVM_Q_SIZE),   intent(inout)   :: qL, qR
            real(kind = GRID_SR), dimension(_FVM_CHUNK_SIZE, _FVM_AUX_SIZE), intent(inout)   :: auxL, auxR
            real(kind = GRID_SR), dimension(_FVM_CHUNK_SIZE, _FVM_Q_SIZE),   intent(inout)   :: fluxL, fluxR
            real(kind = GRID_SR),                                            intent(out)     :: maxWaveSpeed

            ! local
            !real(kind = GRID_SR), DIMENSION(_SWE_CHUNK_SIZE) :: hL, huL, hvL, bL
            real(kind = GRID_SR) :: hL, huL, hvL, bL
            real(kind = GRID_SR) :: hR, huR, hvR, bR
            real(kind = GRID_SR) :: hL2, huL2, hvL2
            real(kind = GRID_SR) :: hR2, huR2, hvR2
            real(kind = GRID_SR) :: upd_hL, upd_huL, upd_hvL, upd_hR, upd_huR, upd_hvR
            real(kind = GRID_SR) :: upd_hL2, upd_huL2, upd_hvL2, upd_hR2, upd_huR2, upd_hvR2
            real(kind = GRID_SR) :: maxWaveSpeedLocal, maxWaveSpeedSolver
            integer :: i
            
            maxWaveSpeedLocal = 0.0_GRID_SR
            
#           if defined(_PATCH_VEC_SIMD) || defined(_PATCH_VEC_INLINE)
                ! Vectorization! (Requires OpenMP 4.0 or later)
                !$OMP SIMD PRIVATE(normals_x,normals_y,hL,hR,huL,huR,hvL,hvR,bL,bR,pL,pR,upd_hL,upd_huL,upd_hvL,upd_hR,upd_huR,upd_hvR,maxWaveSpeedSolver) REDUCTION(max: maxWaveSpeedLocal)
#           endif
            do i=1,numProblems
                ! extract data from Q and AUX input arrays to local variables
                hL  = qL(i,1)
                huL = qL(i,2)
                hvL = qL(i,3)
                hL2 = qL(i,4)
                huL2= qL(i,5)
                hvL2= qL(i,6)
                bL  = auxL(i,1)
                
                hR  = qR(i,1)
                huR = qR(i,2)
                hvR = qR(i,3)
                hR2 = qR(i,4)
                huR2= qR(i,5)
                hvR2= qR(i,6)
                bR  = auxR(i,1)                
            
#               if defined(_PATCH_VEC_INLINE)
                    ! Warning: inlining this subroutine into an OMP SIMD loop may cause
                    ! bugs and incorrect calculations depending on the compiler version
                    ! Check the results!

                    ! Recommended compiler: ifort 17.0

                    !DIR$ FORCEINLINE
#               endif
                call SWE2L_compute_fluxes(normals(i,1), normals(i,2), hL, hR, huL, huR, hvL, hvR, bL, bR, &
                                hL2, hR2, huL2, huR2, hvL2, hvR2, upd_hL, upd_hR, upd_huL, upd_huR, upd_hvL, upd_hvR, &
                                upd_hL2, upd_hR2, upd_huL2, upd_huR2, upd_hvL2, upd_hvR2, maxWaveSpeedSolver)

                maxWaveSpeedLocal = max(maxWaveSpeedLocal, maxWaveSpeedSolver)
                
                ! copy result to output
                fluxL(i,:) = [upd_hL, upd_huL, upd_hvL, upd_hL2, upd_huL2, upd_hvL2]
                fluxR(i,:) = [upd_hR, upd_huR, upd_hvR, upd_hR2, upd_huR2, upd_hvR2]
            end do
            
            maxWaveSpeed = maxWaveSpeedLocal
            
        end subroutine
        
        
        ! ************************************************************************************************************
        ! FVM_timestep_update_cell:
        !
        !   Defines how the Q quantities of a cell will be updated, and whether it should be refined for the next step.
        !   Usually the cell updating step of an FVM simply computes Q <- Q + total_flux * -(dt/cell_volume), but the 
        !   purpose of this operator is to allow more flexibility for handling special cases (e.g., dry cells in SWE).
        !
        !   This operator also defines what should happen to this cell in the next adaptivity step. 
        !
        !   Note: The !$OMP directive in the beginning of the operator body is recommended and usually necessary 
        !   for successful vectorization with the Intel Compiler, if the option PATCH_VEC is set to "SIMD".
        !
        !   Parameters:
        !   (in)    vertices: 3 xy-coordinates, one for each cell's vertex. [real(2,3)]
        !   (inout) Q: quantities of the cell being updated. Note that this is also used as output. [real(_FVM_Q_SIZE)]
        !   (inout) AUX: AUX quantities in the cell. [real(_FVM_AUX_SIZE)]
        !   (in)    total_flux: total sum of the fluxes going into the cell (computed previously with the operator
        !               FVM_timestep_compute_fluxes_single/multi_edge) [real(_FVM_Q_SIZE)]
        !   (in)    dt: dt (delta time) for this timestep. [real]
        !   (in)    cell_volume: volume of this cell (in this 2D application, the cell area). [real]
        !   (out)   refinement_flag: how this cell should be treated in the next time-step. [integer]
        !
        !           Possible values for refinement_flag:
        !            1 -> refine it
        !            0 -> keep it
        !           -1 -> coarsen it
        !           (What will actually happen to this cell will also depend on its neighbor cells and patches)        ! ************************************************************************************************************
        subroutine FVM_timestep_update_cell(vertices, Q, AUX, total_flux, dt, cell_volume, refinement_flag)
#           if defined(_PATCH_VEC_SIMD)
#               if defined(__MIC__) 
                    !$OMP DECLARE SIMD(FVM_timestep_update_cell) simdlen(8) processor(mic) uniform(dt, cell_volume)
#               elif defined(__AVX512F__)
                    !$OMP DECLARE SIMD(FVM_timestep_update_cell) simdlen(8) uniform(dt, cell_volume)
#               else
                    !$OMP DECLARE SIMD(FVM_timestep_update_cell) simdlen(4) uniform(dt, cell_volume)
#               endif
#           endif

            real (kind = GRID_SR), dimension(2,3),              intent(in)      :: vertices
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE),      intent(inout)   :: Q
            real (kind = GRID_SR), dimension(_FVM_AUX_SIZE),    intent(inout)   :: AUX
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE),      intent(in)      :: total_flux
            real (kind = GRID_SR),                              intent(in)      :: dt
            real (kind = GRID_SR),                              intent(in)      :: cell_volume
            integer,                                            intent(out)     :: refinement_flag
            
            !local
            real (kind = GRID_SR) :: dQ_max_norm
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE) :: dQ ! delta Q
            
            ! Following the original SWE/SWE2L implementations, the refinement_flag is defined based on the total_flux
            dQ_max_norm = max(abs(total_flux(1)),abs(total_flux(4)))
            refinement_flag = 0
            if (dQ_max_norm > 0.02_GRID_SR * cfg%scaling * get_edge_size(cfg%i_max_depth) / _PATCH_BOUNDARY_SIZE ) then
                refinement_flag = 1
            else if (dQ_max_norm < 0.02_GRID_SR * cfg%scaling * get_edge_size(cfg%i_max_depth) / (_PATCH_BOUNDARY_SIZE * 8.0_SR) ) then
                refinement_flag = -1
            endif
  
            ! I don't do this in SWE2L! 
                ! if land is flooded, init water height to dry tolerance and
                ! velocity to zero
!                     where (data%H < data%H2 + cfg%dry_tolerance .and. dQ_H > 0.0_GRID_SR)
!                         data%H = data%H2 + cfg%dry_tolerance
!                         data%HU = 0.0_GRID_SR
!                         data%HV = 0.0_GRID_SR
!                     end where
!                     where (data%H2 < data%B + cfg%dry_tolerance .and. dQ_H2 > 0.0_GRID_SR)
!                         data%H2 = data%B + cfg%dry_tolerance
!                         data%HU2 = 0.0_GRID_SR
!                         data%HV2 = 0.0_GRID_SR
!                     end where

            ! Update cell quantities
            dq = total_flux * (-dt/cell_volume)
            Q = Q + dq
            
            ! Special case: water height of the top layer is also influenced by variation of water height of the bottom layer
            Q(1) = Q(1) + dQ(4)
                
            ! Handle cell wetting: if the water level falls below the dry tolerance, set water level to 0 and velocity to 0
            if (dQ(4) < 0.0_GRID_SR .and. Q(4) < AUX(1) + cfg%dry_tolerance) then ! dq_h2 < 0 .and. h2 < b + drytol
                Q(4) = AUX(1) ! h2 = b
                Q(5) = 0.0_GRID_SR ! hu2 = 0
                Q(6) = 0.0_GRID_SR ! hv2 = 0
            end if
            if (dQ(1) < 0.0_GRID_SR .and. Q(1) < Q(4) + cfg%dry_tolerance) then ! dq_h < 0 .and. h < h2 + drytol
                Q(1) = Q(4) ! h = h2
                Q(2) = 0.0_GRID_SR ! hu = 0
                Q(3) = 0.0_GRID_SR ! hv = 0
            end if

        end subroutine
        
        
        
! ################################################################################################################
! # Operators required for FVM_adapt:
! ################################################################################################################
        
        
        ! ************************************************************************************************************
        ! FVM_adapt_split_cell:
        !
        !   Defines how a coarse cell should be split into two new, finer ones. This usually creates two new cells with
        !   the same quantity values as the original coarse on, but in some cases it may be useful to access external 
        !   data (e.g., bathymetry in SWE) or to handle special cases (e.g., dry cells in SWE).
        !
        !
        !   The data for the original coarse cell is stored in Q and AUX.
        !   The data for the new, finer cells needs to be stored in Q_split1 and AUX_split1 for the first cell and
        !   in Q_split2 and AUX_split2 for the second cell.
        !
        !   Parameters:
        !   (in)    Q: Q quantities for the original coarse cell. [real(_FVM_Q_SIZE)]
        !   (in)    AUX: AUX quantities for the original coarse cell. [real(_FVM_AUX_SIZE)]
        !   (in)    vertices1 : 3 xy-coordinates, one for each vertex in the first new cell. [real(2,3)]
        !   (in)    vertices2 : 3 xy-coordinates, one for each vertex in the second new cell. [real(2,3)]
        !   (out)   Q_split1: Q quantities for the first new (finer) cell. [real(_FVM_Q_SIZE)]
        !   (out)   Q_split2: Q quantities for the second new (finer) cell. [real(_FVM_Q_SIZE)]
        !   (out)   AUX_split1: AUX quantities for the first new (finer) cell. [real(_FVM_AUX_SIZE)]
        !   (out)   AUX_split2: AUX quantities for the second new (finer) cell. [real(_FVM_AUX_SIZE)]
        ! ************************************************************************************************************
        subroutine FVM_adapt_split_cell(Q, AUX, vertices1, vertices2, Q_split1, Q_split2, AUX_split1, AUX_split2)
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(in)      :: Q
            real(kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(in)      :: AUX
            real(kind = GRID_SR), dimension(2,3),           intent(in)      :: vertices1, vertices2
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(out)     :: Q_split1, Q_split2
            real(kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(out)     :: AUX_split1, AUX_split2
            
            !local 
            real (kind = GRID_SR), dimension(3) :: barycenter
            
            ! For AUX we ignore the input cells and instead query the bathymetry data for barycenter of the new cell
            barycenter(1:2) = (vertices1(:,1) + vertices1(:,2) + vertices1(:,3)) / 3.0_GRID_SR
            AUX_split1(1) = FVM_SWE2L_get_bathymetry(barycenter)
            barycenter(1:2) = (vertices2(:,1) + vertices2(:,2) + vertices2(:,3)) / 3.0_GRID_SR
            AUX_split2(1) = FVM_SWE2L_get_bathymetry(barycenter)

            ! For Q we simply use the same values as the original cell, but take care of dry cells
            ! Basic rule: if the cell was dry, the water height in the new cells should be either dry 
            ! or at a baseline initial level (e.g., sea-level)
            Q_split1 = Q
            Q_split2 = Q
            
            ! Dry layers whenever necessary
            Q_split1(4) = max(Q_split1(4), AUX_split1(1))
            Q_split1(1) = max(Q_split1(1), Q_split1(4))
            Q_split2(4) = max(Q_split2(4), AUX_split2(1))
            Q_split2(1) = max(Q_split2(1), Q_split2(4))
            
            ! check if second layer should be dry or at baseline level
            if (Q(4) < AUX(1) + cfg%dry_tolerance) then ! if h2 < b + drytol              
                Q_split1(4) = max (AUX_split1(1), FVM_SWE2L_get_baseline_water_level(2)) ! h2 = max(b, baseline(second_layer))
                Q_split1(5) = 0.0_GRID_SR !hu2 = 0
                Q_split1(6) = 0.0_GRID_SR !hv2 = 0
                
                Q_split2(4) = max (AUX_split2(1), FVM_SWE2L_get_baseline_water_level(2)) ! h2 = max(b, baseline(second_layer))
                Q_split2(5) = 0.0_GRID_SR !hu2 = 0
                Q_split2(6) = 0.0_GRID_SR !hv2 = 0
            end if
            ! check if first layer should be dry or at baseline level
            if (Q(1) < Q(4) + cfg%dry_tolerance) then ! if h1 < h2 + drytol              
                Q_split1(1) = max (Q_split1(4), FVM_SWE2L_get_baseline_water_level(1)) ! h1 = max(h2, baseline(first_layer))
                Q_split1(2) = 0.0_GRID_SR ! hu1 = 0
                Q_split1(3) = 0.0_GRID_SR ! hv1 = 0
                
                Q_split2(1) = max (Q_split2(4), FVM_SWE2L_get_baseline_water_level(1)) ! h1 = max(h2, baseline(first_layer))
                Q_split2(2) = 0.0_GRID_SR ! hu1 = 0
                Q_split2(3) = 0.0_GRID_SR ! hv1 = 0
            end if
        
        end subroutine
        
        
        ! ************************************************************************************************************
        ! FVM_adapt_merge_cells:
        !
        !   Defines how two neighbor cells should be merged to form a new, coarser one. This is usually the average of
        !   the cells' values, but in some cases it may be useful to access external data (e.g., bathymetry in SWE)
        !   or to handle special cases (e.g., dry cells in SWE).
        !
        !   The first cell's data is stored in Q1 and AUX1, and the second cell's data in Q2 and AUX2.
        !   The data for the new, coarse cell needs to be stored in Q_merged and AUX_merged. 
        !
        !   Note: The order of the input cells does not really matter.
        !
        !   Parameters:
        !   (in)    Q1: Q quantities for the first cell. [real(_FVM_Q_SIZE)]
        !   (in)    Q2: Q quantities for the second cell. [real(_FVM_Q_SIZE)]
        !   (in)    aux1: AUX quantities for the first cell. [real(_FVM_AUX_SIZE)]
        !   (in)    aux2: AUX quantities for the second cell. [real(_FVM_AUX_SIZE)]
        !   (in)    vertices: 3 xy-coordinates, one for each vertex in the new cell. [real(2,3)]
        !   (out)   Q_merged: Q quantities for the new, coarser cell. [real(_FVM_Q_SIZE)]
        !   (out)   AUX_merged: AUX quantities for the new, coarser cell. [real(_FVM_AUX_SIZE)]
        ! ************************************************************************************************************
        subroutine FVM_adapt_merge_cells(Q1, Q2, AUX1, AUX2, vertices, Q_merged, AUX_merged)
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(in)      :: Q1, Q2
            real(kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(in)      :: AUX1, AUX2
            real(kind = GRID_SR), dimension(2,3),           intent(in)      :: vertices
            real(kind = GRID_SR), dimension(_FVM_Q_SIZE),   intent(out)     :: Q_merged
            real(kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(out)     :: AUX_merged
            
            !local 
            real (kind = GRID_SR), dimension(3) :: barycenter
            
            ! compute cell barycenter
            barycenter(1:2) = (vertices(:,1) + vertices(:,2) + vertices(:,3)) / 3.0_GRID_SR
            barycenter(3) = 0.0_GRID_SR ! z dimension is not used

            ! For AUX we ignore the input cells and instead query the bathymetry data for barycenter of the new cell
            AUX_merged(1) = FVM_SWE2L_get_bathymetry(barycenter)

            ! For Q we simply use the average values, but take care of dry cells
            ! Basic rule: if one of the cells is dry, the water height in the new cell should be either dry 
            ! or at a baseline initial level (e.g., sea-level)
            Q_merged = 0.5 * (Q1 + Q2)
            
            ! check if second layer should be dry or at baseline level
            if (Q1(4) < AUX1(1) + cfg%dry_tolerance .or. Q2(4) < AUX2(1) + cfg%dry_tolerance) then ! if h2 < b + drytol              
                Q_merged(4) = max (AUX_merged(1), FVM_SWE2L_get_baseline_water_level(2)) ! h2 = max(b, baseline(second_layer))
                Q_merged(5) = 0.0_GRID_SR !hu2 = 0
                Q_merged(6) = 0.0_GRID_SR !hv2 = 0
            end if
            ! check if first layer should be dry or at baseline level
            if (Q1(1) < Q1(4) + cfg%dry_tolerance .or. Q2(1) < Q2(4) + cfg%dry_tolerance) then ! if h1 < h2 + drytol              
                Q_merged(1) = max (Q_merged(4), FVM_SWE2L_get_baseline_water_level(1)) ! h1 = max(h2, baseline(first_layer))
                Q_merged(2) = 0.0_GRID_SR ! hu1 = 0
                Q_merged(3) = 0.0_GRID_SR ! hv1 = 0
            end if
        
        end subroutine
        
        
        
! ################################################################################################################
! # Subroutines required for FVM_xml_output:
! ################################################################################################################
            
            
        ! ************************************************************************************************************
        ! FVM_output_pre_process_data: 
        !
        !   Used to pre-process the data before writing the XML output. 
        !   E.g., here we can check for dry cells and set them as NaN or any other value.
        !   If no processing is desired, this should simply copy Q to Q_out and AUX to AUX_out (required!!)
        !
        !   Parameters:
        !   (in)    Q: Q quantities in the cell. [real(_FVM_Q_SIZE)]
        !   (in)    AUX: AUX quantities in the cell. [real(_FVM_AUX_SIZE)]
        !   (out)   Q_out: Q quantities to be written to the output. [real(_FVM_Q_SIZE)]
        !   (out)   AUX_out: AUX quantities to be written to the output. [real(_FVM_AUX_SIZE)]
        ! ************************************************************************************************************
        subroutine FVM_output_pre_process_data(Q, AUX, Q_out, AUX_out)
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE),      intent(in)      :: Q
            real (kind = GRID_SR), dimension(_FVM_AUX_SIZE),    intent(in)      :: AUX
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE),      intent(out)     :: Q_out
            real (kind = GRID_SR), dimension(_FVM_AUX_SIZE),    intent(out)     :: AUX_out
            
            !local
            real (kind = GRID_SR) :: h1, hu1, hv1, h2, hu2, hv2
            real (kind = GRID_SR) :: b
            real (kind = GRID_SR) :: NaN
            
            ! Assign NaN to this -> will be used for dry cells/layers
            NaN = 0.0
#           if !defined(_DEBUG)            
                NaN = 0.0/NaN
#           endif

            ! copy data to local variables
            call FVM_SWE2L_extract_Q_data(Q, h1, hu1, hv1, h2, hu2, hv2)
            call FVM_SWE2L_extract_AUX_data(AUX, b)
            
            ! set water height of dry cells to NAN -> top layer
            if (h1 < h2 + cfg%dry_tolerance .or. h1 < b + cfg%dry_tolerance ) then
                h1 = NaN
                hu1 = NaN
                hv1 = NaN
            end if
            ! set water height of dry cells to NAN -> bottom layer
            if (h2 < b + cfg%dry_tolerance ) then
                h2 = NaN
                hu2 = NaN
                hv2 = NaN
            end if
            
            ! create output arrays
            Q_out = [h1, hu1, hv1, h2, hu2, hv2]
            AUX_out = [b]
            
            ! Simply uncomment and use code below if no pre-processing of output is required:
            !
            ! Q_out = Q
            ! AUX_out = AUX
        end subroutine
                
                
        ! ************************************************************************************************************
        ! FVM_output_labels: 
        !   the output labels for each cell quantity (Q and AUX) are defined here.
        !
        !   Parameters:
        !   (out)   Q_labels: Q quantities to be written to the output. [array with _FVM_Q_SIZE strings of size=32]
        !   (out)   AUX_out: AUX quantities to be written to the output. [array with _FVM_AUX_SIZE strings of size=32]
        ! ************************************************************************************************************
        subroutine FVM_output_labels(Q_labels, AUX_labels)
            character(len=32), dimension(_FVM_Q_SIZE),      intent(out) :: Q_labels
            character(len=32), dimension(_FVM_AUX_SIZE),    intent(out) :: AUX_labels
            
            Q_labels(1) = "(1) Water height (top layer)"
            Q_labels(2) = "(2) hu (top layer)"
            Q_labels(3) = "(3) hv (top layer)"
            Q_labels(4) = "(4) Water height  (bottom layer)"
            Q_labels(5) = "(5) hu (bottom layer)"
            Q_labels(6) = "(6) hv (bottom layer)"
                        
            AUX_labels(1) = "(7) Bathymetry"
                             
        end subroutine
        
        

!################################################################################################################
!# "Private" subroutines: 
!#   all subroutines below were created specifially for this scenario and are considered "local" or "private".
!#   They don't need to be defined for other scenarios, although some of them can be extremely useful, e.g.
!#   "FVM_SWE_extract_Q_data". Note that their parameters can be entirely scenario-dependent.
!# ##############################################################################################################
        
        ! extracts data from Q array to scalar variables h, hu, hv...
        subroutine FVM_SWE2L_extract_Q_data(Q, h1, hu1, hv1, h2, hu2, hv2)
            real (kind = GRID_SR), dimension(_FVM_Q_SIZE), intent(in) :: Q
            real (kind = GRID_SR), intent(out) :: h1, hu1, hv1, h2, hu2, hv2
            
            h1  = Q(1)
            hu1 = Q(2)
            hv1 = Q(3)
            h2  = Q(4)
            hu2 = Q(5)
            hv2 = Q(6)        
        end subroutine
        
        ! extracts data from AUX array to scalar variable b
        subroutine FVM_SWE2L_extract_AUX_data(AUX, b)
            real (kind = GRID_SR), dimension(_FVM_AUX_SIZE), intent(in)     :: AUX
            real (kind = GRID_SR), intent(out) :: b
        
            b = AUX(1)
        end subroutine
        
        ! change base so hu/hv become orthogonal/perperdicular to edge
        subroutine FVM_SWE2L_apply_transformations_before(transform_matrix, hu, hv)
#           if defined(_PATCH_VEC_SIMD)
                !$OMP DECLARE SIMD(FVM_SWE2L_apply_transformations_before)
#           endif
            real(kind = GRID_SR), intent(in)         :: transform_matrix(2,2)
            real(kind = GRID_SR), intent(inout)      :: hu, hv           
            
            real(kind = GRID_SR)                     :: temp
            
            temp = hu
            hu = transform_matrix(1,1) * hu + transform_matrix(1,2) * hv
            hv = transform_matrix(2,1) * temp + transform_matrix(2,2) * hv
        end subroutine
        
        ! transform back to original base
        subroutine FVM_SWE2L_apply_transformations_after(transform_matrix, hu, hv)
#           if defined(_PATCH_VEC_SIMD)
                !$OMP DECLARE SIMD(FVM_SWE2L_apply_transformations_after)
#           endif
            real(kind = GRID_SR), intent(in)         :: transform_matrix(2,2)
            real(kind = GRID_SR), intent(inout)      :: hu, hv           
            
            real(kind = GRID_SR)                     :: temp
            
            temp = hu
            hu = transform_matrix(1,1) * hu + transform_matrix(2,1) * hv
            hv = transform_matrix(1,2) * temp + transform_matrix(2,2) * hv
        end subroutine
        
        ! baseline level for the water in each layer (e.g., sea-water level), necessary for mantaining water-at-rest states during refinement/coarsening
        function FVM_SWE2L_get_baseline_water_level(layer) result(h)
            integer :: layer
            real (kind = GRID_SR) :: h
            
            if (layer == 2) then
#               if defined (_ASAGI)
                    h = -500.0_GRID_SR
#               else
                    h = -20.0_GRID_SR
#               endif
            else
                h = -0.0_GRID_SR
            end if
        end function
        
        ! get bathymetry either from external file (using ASAGI) or from SWE2L_Scenario
        function FVM_SWE2L_get_bathymetry(x) result(bathymetry)
            real (kind = c_double), intent(in) :: x(3)
            real (kind = GRID_SR) :: bathymetry

#           if defined (_ASAGI)
                ! get bathymetry data from file with ASAGI
                if (asagi_grid_min(cfg%afh_bathymetry, 0) <= x(1) .and. asagi_grid_min(cfg%afh_bathymetry, 1) <= x(2) &
                        .and. x(1) <= asagi_grid_max(cfg%afh_bathymetry, 0) .and. x(2) <= asagi_grid_max(cfg%afh_bathymetry, 1)) then

                    bathymetry = asagi_grid_get_float(cfg%afh_bathymetry, x, 0)
                    
                    ! also consider displacement
                    if (asagi_grid_min(cfg%afh_displacement, 0) <= x(1) .and. asagi_grid_min(cfg%afh_displacement, 1) <= x(2) &
                        .and. x(1) <= asagi_grid_max(cfg%afh_displacement, 0) .and. x(2) <= asagi_grid_max(cfg%afh_displacement, 1)) then
                        
                        bathymetry = bathymetry + asagi_grid_get_float(cfg%afh_displacement, x, 0)
                    end if
                else
                    bathymetry = -5000.0_SR !we assume that the sea floor is constant here
                end if
#           else
                ! Use routine from SWE2L_Scenario module here (from src/SWE2L)
                bathymetry = SWE2L_Scenario_get_bathymetry(x)
#           endif
        end function
        
        
        function FVM_SWE2L_get_initial_Q(x) result (Q)
            real (kind = c_double), intent(in) :: x(3)
            real (kind = GRID_SR), dimension(6) :: Q ! Quantities array: [h1, hu1, hv1, h2, hu2, hv2]

#           if defined (_ASAGI)
                ! get displacement data from file with ASAGI
                if (asagi_grid_min(cfg%afh_displacement, 0) <= x(1) .and. asagi_grid_min(cfg%afh_displacement, 1) <= x(2) &
                        .and. x(1) <= asagi_grid_max(cfg%afh_displacement, 0) .and. x(2) <= asagi_grid_max(cfg%afh_displacement, 1)) then

                    Q(1) = asagi_grid_get_float(cfg%afh_displacement, x, 0) ! h1
                else
                    Q(1) = FVM_SWE2L_get_baseline_water_level(1) ! h1
                end if
                
                
                ! other quantities are constant at t=0
                Q(2:3) = 0.0_GRID_SR ! hu1 and hv1
                Q(4) = FVM_SWE2L_get_baseline_water_level(2) ! h2
                Q(5:6) = 0.0_GRID_SR ! hu2 and hv2
#           else
                ! Use routine from SWE2L_Scenario module here (from src/SWE2L)
                Q = SWE2L_Scenario_get_initial_Q_array(x)
#           endif
        end function

#endif
    
    END MODULE
