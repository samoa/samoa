#if !defined(__FVM_SWE2L_MACROS_F90)

#   define __FVM_SWE2L_MACROS_F90

#   if defined(_FVM_SWE2L)
        ! List of unknowns for SWE2L:
        ! 1 = h1   (water height, top level)
        ! 2 = hu1  (water momentum in x direction, top layer)
        ! 3 = hv1  (water momentum in y direction, top layer)
        ! 4 = h2   (water height, bottom level)
        ! 5 = hu2  (water momentum in x direction, bottom layer)
        ! 6 = hv2  (water momentum in y direction, bottom layer)
#       define _FVM_Q_SIZE 6

        ! List of aux variables for SWE2L:
        ! 1 = b    (bathymetry)        
#       define _FVM_AUX_SIZE 1

        ! Custom chunk size:
#       define _FVM_CHUNK_SIZE 16

#   endif

#endif
