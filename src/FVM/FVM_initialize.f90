! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE

#include "Compilation_control.f90"

#if defined(_FVM)
	MODULE FVM_Initialize_Dofs
 		use Tools_noise

        use iso_c_binding
		use SFC_edge_traversal
		use FVM_euler_timestep
		use Samoa
		
        use Tools_patch
        use FVM_module_wrapper

		implicit none

        type num_traversal_data
            integer (kind = GRID_DI)			:: i_refinements_issued
        end type

#		define _GT_NAME							t_fvm_init_dofs_traversal

#		define _GT_EDGES
#		define _GT_EDGES_TEMP

#		define _GT_PRE_TRAVERSAL_OP				pre_traversal_op
#		define _GT_PRE_TRAVERSAL_GRID_OP		pre_traversal_grid_op
#		define _GT_POST_TRAVERSAL_GRID_OP		post_traversal_grid_op
#		define _GT_ELEMENT_OP					element_op

#		define _GT_CELL_TO_EDGE_OP				cell_to_edge_op

#		include "SFC_generic_traversal_ringbuffer.f90"

		subroutine pre_traversal_grid_op(traversal, grid)
			type(t_fvm_init_dofs_traversal), intent(inout)		        :: traversal
			type(t_grid), intent(inout)							    :: grid

			grid%r_time = 0.0_GRID_SR
			grid%r_dt = 0.0_GRID_SR
			grid%r_dt_new = 0.0_GRID_SR

            call scatter(grid%r_time, grid%sections%elements_alloc%r_time)
		end subroutine

		subroutine post_traversal_grid_op(traversal, grid)
			type(t_fvm_init_dofs_traversal), intent(inout)		        :: traversal
			type(t_grid), intent(inout)							    :: grid

            call reduce(traversal%i_refinements_issued, traversal%sections%i_refinements_issued, MPI_SUM, .true.)
            call reduce(grid%r_dt_new, grid%sections%elements_alloc%r_dt_new, MPI_MIN, .true.)

            grid%r_dt = cfg%courant_number * grid%r_dt_new
		end subroutine

		subroutine pre_traversal_op(traversal, section)
			type(t_fvm_init_dofs_traversal), intent(inout)				    :: traversal
			type(t_grid_section), intent(inout)							:: section

			!this variable will be incremented for each cell with a refinement request
			traversal%i_refinements_issued = 0
			section%r_dt_new = huge(1.0_GRID_SR)
		end subroutine

		!******************
		!Geometry operators
		!******************

		subroutine element_op(traversal, section, element)
			type(t_fvm_init_dofs_traversal), intent(inout)				    :: traversal
			type(t_grid_section), intent(inout)							:: section
			type(t_element_base), intent(inout)					        :: element
            
            real (kind = GRID_SR), DIMENSION(2,3)                           :: vertices
            real (kind = GRID_SR), DIMENSION(_FVM_Q_SIZE)                   :: Q
            real (kind = GRID_SR), DIMENSION(_FVM_AUX_SIZE)                 :: AUX
            real (kind = GRID_SR), DIMENSION(_PATCH_NUM_CELLS)       :: estimated_wave_speed
            integer (kind = BYTE), DIMENSION(_PATCH_NUM_CELLS)       :: refinement_flag
            real (kind = GRID_SR)                                           :: max_wave_speed
            
            integer (kind = GRID_SI)                                        :: i, j, row, col, cell_id
            
            ! loop through all cells in the patch initializing them
            do i=1, _PATCH_NUM_CELLS
            
                ! get real world coordinates for the three vertices
                vertices = patch_geometry%get_vertices(i, int(element%cell%geometry%i_plotter_type))
                do j=1,3
                    vertices(:,j) = samoa_barycentric_to_world_point(element%transform_data, vertices(:,j))
                    vertices(:,j) = vertices(:,j) * cfg%scaling + cfg%offset
                end do

                ! *** CALL TO AN USER-DEFINED FUNCTION ***
                call FVM_initialize_cell(vertices, Q, AUX, estimated_wave_speed(i), refinement_flag(i))
                
                ! copy values to cell data
                element%cell%data_pers%Q(i,:) = Q
                element%cell%data_pers%AUX(i,:) = AUX

                ! increment for next iteration
                col = col + 1
                if (col == 2*row) then
                    col = 1
                    row = row + 1
                end if
            end do
            
            ! estimate initial time step
            max_wave_speed = maxval(estimated_wave_speed(:))
            if (max_wave_speed .ne. 0.0_GRID_SR) then
                section%r_dt_new = min(section%r_dt_new, element%cell%geometry%get_volume() / (sum(element%cell%geometry%get_edge_sizes()) * max_wave_speed * _PATCH_BOUNDARY_SIZE))
            end if

            ! refine this patch if any cell was flagged as "refine"
            if (any(refinement_flag(:) == 1)) then
                element%cell%geometry%refinement = 1
            else
                element%cell%geometry%refinement = 0
            end if
            
            ! check whether the refinement flag defined above is valid (compare with min and max depth)
			if (element%cell%geometry%i_depth < cfg%i_min_depth) then
                ! always refine if the minimum depth is not met
 				element%cell%geometry%refinement = 1
				traversal%i_refinements_issued = traversal%i_refinements_issued + 1
            else if (element%cell%geometry%i_depth < cfg%i_max_depth) then
                ! refinement is allowed
                if (element%cell%geometry%refinement == 1) then
                    traversal%i_refinements_issued = traversal%i_refinements_issued + 1
                end if
            else ! max_depth was reached, refinement not allowed!
                element%cell%geometry%refinement = 0
            end if
            
		end subroutine
		
	END MODULE
#endif

