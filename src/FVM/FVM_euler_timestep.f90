! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE


#include "Compilation_control.f90"

#if defined(_FVM)
	MODULE FVM_Euler_Timestep
		use SFC_edge_traversal

		use Samoa
		use c_bind_riemannsolvers
		
		use Tools_patch
        use FVM_module_wrapper
        
		implicit none

        type num_traversal_data
            integer (kind = GRID_DI)			:: i_refinements_issued
        end type

        interface skeleton_op
            module procedure skeleton_array_op
            module procedure skeleton_scalar_op
        end interface

        interface bnd_skeleton_op
            module procedure bnd_skeleton_array_op
            module procedure bnd_skeleton_scalar_op
        end interface

		PUBLIC cell_to_edge_op


#		define _GT_NAME							t_fvm_euler_timestep_traversal

#		define _GT_EDGES

#		define _GT_PRE_TRAVERSAL_OP				pre_traversal_op
#		define _GT_PRE_TRAVERSAL_GRID_OP		pre_traversal_grid_op
#		define _GT_POST_TRAVERSAL_GRID_OP		post_traversal_grid_op

#		define _GT_CELL_TO_EDGE_OP				cell_to_edge_op
#		define _GT_SKELETON_OP					skeleton_op
#		define _GT_BND_SKELETON_OP				bnd_skeleton_op
#		define _GT_CELL_UPDATE_OP				cell_update_op
#		define _GT_CELL_LAST_TOUCH_OP			cell_last_touch_op
#		define _GT_NODE_WRITE_OP			    node_write_op
#		define _GT_EDGE_WRITE_OP			    edge_write_op

#		define _GT_NODE_MPI_TYPE

#		include "SFC_generic_traversal_ringbuffer.f90"

        subroutine create_node_mpi_type(mpi_node_type)
            integer, intent(out)            :: mpi_node_type

#           if defined(_MPI)
                type(t_node_data)                       :: node
                integer                                 :: blocklengths(2), types(2), disps(2), type_size, i_error
                integer (kind = MPI_ADDRESS_KIND)       :: lb, ub

                blocklengths(1) = 1
                blocklengths(2) = 1

                disps(1) = 0
                disps(2) = sizeof(node)

                types(1) = MPI_LB
                types(2) = MPI_UB

                call MPI_Type_struct(2, blocklengths, disps, types, mpi_node_type, i_error); assert_eq(i_error, 0)
                call MPI_Type_commit(mpi_node_type, i_error); assert_eq(i_error, 0)

                call MPI_Type_size(mpi_node_type, type_size, i_error); assert_eq(i_error, 0)
                call MPI_Type_get_extent(mpi_node_type, lb, ub, i_error); assert_eq(i_error, 0)

                assert_eq(0, lb)
                assert_eq(0, type_size)
                assert_eq(sizeof(node), ub)
#           endif
        end subroutine

		!*******************************
		!Geometry operators
		!*******************************

		subroutine pre_traversal_grid_op(traversal, grid)
			type(t_fvm_euler_timestep_traversal), intent(inout)		:: traversal
			type(t_grid), intent(inout)							    :: grid

			if (cfg%r_max_time > 0.0_SR) then
                grid%r_dt = min(cfg%r_max_time, grid%r_dt)
            end if

			if (cfg%r_output_time_step > 0.0_SR) then
                grid%r_dt = min(cfg%r_output_time_step, grid%r_dt)
            end if

#           if defined(_ASAGI)
                !if we are in the earthquake phase, limit the simulation time step by the earthquake time step
                if (grid%r_time < cfg%t_max_eq) then
                    grid%r_dt = min(grid%r_dt, cfg%dt_eq)
                end if
#           endif

			call scatter(grid%r_dt, grid%sections%elements_alloc%r_dt)
		end subroutine

		subroutine post_traversal_grid_op(traversal, grid)
			type(t_fvm_euler_timestep_traversal), intent(inout)		:: traversal
			type(t_grid), intent(inout)							    :: grid

			grid%r_time = grid%r_time + grid%r_dt

            call reduce(traversal%i_refinements_issued, traversal%sections%i_refinements_issued, MPI_SUM, .true.)
            call reduce(grid%r_dt_new, grid%sections%elements_alloc%r_dt_new, MPI_MIN, .true.)

            grid%r_dt_new = cfg%courant_number * grid%r_dt_new

            if (rank_MPI == 0) then
                if (cfg%courant_number > grid%r_dt_new / grid%r_dt) then
                    _log_write(1, '("WARNING! Time step size was too big. dt (old): ", ES10.3, ", dt (CFL): ", ES10.3, ", maximum courant number: ", F0.3)') grid%r_dt, grid%r_dt_new / cfg%courant_number, grid%r_dt_new / grid%r_dt
                end if
            end if

            grid%r_dt = grid%r_dt_new

			call scatter(grid%r_time, grid%sections%elements_alloc%r_time)
		end subroutine

		subroutine pre_traversal_op(traversal, section)
			type(t_fvm_euler_timestep_traversal), intent(inout)				:: traversal
			type(t_grid_section), intent(inout)							:: section

			!this variable will be incremented for each cell with a refinement request
			traversal%i_refinements_issued = 0_GRID_DI
			section%r_dt_new = huge(1.0_SR)
		end subroutine

		function cell_to_edge_op(element, edge) result(rep)
			type(t_element_base), intent(in)						:: element
			type(t_edge_data), intent(in)						    :: edge
			type(num_cell_rep)										:: rep

            integer(kind = GRID_SI)                                 :: i
            integer                                                 :: edge_type !1=left, 2=hypotenuse, 3=right
            integer                                                 :: cell_id

            !find out which edge it is comparing its normal with cell normals
            ! obs.: negative plotter_types describe triangles in desired order: left, hypotenuse, right
            associate(cell_edge => ref_plotter_data(- abs(element%cell%geometry%i_plotter_type))%edges, normal => edge%transform_data%normal)
                do i=1,3
                    if ((normal(1) == cell_edge(i)%normal(1) .and. normal(2) == cell_edge(i)%normal(2))   &
                            .or. (normal(1) == -cell_edge(i)%normal(1) .and. normal(2) == -cell_edge(i)%normal(2))) then
                        edge_type = i
                    end if
                end do
            end associate
            
            associate(data => element%cell%data_pers)
                ! copy boundary values to respective edges
                ! left leg cells go to edge 1
                ! hypotenuse cells go to edge 2
                ! right leg cells go to edge 3
                
                do i=1, _PATCH_BOUNDARY_SIZE
                    cell_id = patch_geometry%get_boundary_cell(edge_type, i)
                    rep%Q (i,:) = data%Q (cell_id,:)
                    rep%AUX(i,:) = data%AUX(cell_id,:)
                end do
            end associate
                
		end function

		subroutine skeleton_array_op(traversal, grid, edges, rep1, rep2, update1, update2)
			type(t_fvm_euler_timestep_traversal), intent(in)				:: traversal
			type(t_grid_section), intent(in)							    :: grid
			type(t_edge_data), intent(in)								    :: edges(:)
			type(num_cell_rep), intent(in)									:: rep1(:), rep2(:)
			type(num_cell_update), intent(out)								:: update1(:), update2(:)

            integer (kind = GRID_SI)                                        :: i

            do i = 1, size(edges)
                call skeleton_scalar_op(traversal, grid, edges(i), rep1(i), rep2(i), update1(i), update2(i))
            end do
		end subroutine

		subroutine skeleton_scalar_op(traversal, grid, edge, rep1, rep2, update1, update2)
			type(t_fvm_euler_timestep_traversal), intent(in)				:: traversal
			type(t_grid_section), intent(in)							    :: grid
			type(t_edge_data), intent(in)								    :: edge
			type(num_cell_rep), intent(in)									:: rep1, rep2
			type(num_cell_update), intent(out)								:: update1, update2
			integer                                                         :: i

            ! invert values in edges
            ! cells are copied in inverse order because the neighbor 
            ! ghost cells will have a mirrored numbering! See a (poorly-drawn) example:
            !         ___
            !  /\3   1\  |
            ! /  \2   2\ |
            !/____\1   3\|
            
            do i=1, _PATCH_BOUNDARY_SIZE
                update1%Q (i,:) = rep2%Q (_PATCH_BOUNDARY_SIZE + 1 - i, :)
                update1%AUX(i,:) = rep2%AUX(_PATCH_BOUNDARY_SIZE + 1 - i, :)
                
                update2%Q (i,:) = rep1%Q (_PATCH_BOUNDARY_SIZE + 1 - i, :)
                update2%AUX(i,:) = rep1%AUX(_PATCH_BOUNDARY_SIZE + 1 - i, :)
            end do

		end subroutine

		subroutine bnd_skeleton_array_op(traversal, grid, edges, rep, update)
			type(t_fvm_euler_timestep_traversal), intent(in)				:: traversal
			type(t_grid_section), intent(in)							    :: grid
			type(t_edge_data), intent(in)								    :: edges(:)
			type(num_cell_rep), intent(in)									:: rep(:)
			type(num_cell_update), intent(out)								:: update(:)

            integer (kind = GRID_SI)                                        :: i

            do i = 1, size(edges)
                call bnd_skeleton_scalar_op(traversal, grid, edges(i), rep(i), update(i))
            end do
		end subroutine

		subroutine bnd_skeleton_scalar_op(traversal, grid, edge, rep, update)
			type(t_fvm_euler_timestep_traversal), intent(in)				:: traversal
			type(t_grid_section), intent(in)							    :: grid
			type(t_edge_data), intent(in)								    :: edge
			type(num_cell_rep), intent(in)									:: rep
			type(num_cell_update), intent(out)								:: update

			type(num_cell_rep)												:: bnd_rep
			type(num_cell_update)											:: bnd_flux
			integer                                                         :: i

            !SLIP: reflect momentum at normal
			!bnd_rep = t_state(rep%Q(1)%h, rep%Q(1)%p - dot_product(rep%Q(1)%p, edge%transform_data%normal) * edge%transform_data%normal, rep%Q(1)%b)

            !NOSLIP: invert momentum (stable)
			!bnd_rep = t_state(rep%Q(1)%h, -rep%Q(1)%p, rep%Q(1)%b)

			!OUTFLOW: copy values
			bnd_rep = rep

            ! boundary conditions on ghost cells
            update%Q(:,:)  = bnd_rep%Q(:,:)
            update%AUX(:,:) = bnd_rep%AUX(:,:)
            
		end subroutine
		

		
		subroutine cell_update_op(traversal, section, element, update1, update2, update3)
			type(t_fvm_euler_timestep_traversal), intent(inout)				:: traversal
			type(t_grid_section), intent(inout)							:: section
			type(t_element_base), intent(inout)						:: element
			type(num_cell_update), intent(inout)						:: update1, update2, update3

            ! To avoid having very large arrays here and reduce cache misses, Riemann problems are actually
            ! solved in "chunks", similar to cache-blocking strategies. This is especially important for large patches
            ! and architectures with small cache sizes (e.g. KNC, KNL). 
            ! Values from 32 to 128 usually give reasonable performance here.
            
            ! NOTE: _FVM_CHUNK_SIZE is now defined in Compilation_control.f90 !

            integer                                                         :: i, j, ind, edgesLeft, numProblemsInChunk
            type(num_cell_update)                                           :: tmp !> ghost cells in correct order 
            real(kind = GRID_SR)                                            :: volume, edge_lengths(3), maxWaveSpeed, maxWaveSpeedLocal, dQ_max_norm, dt_div_volume
            real(kind = GRID_SR), DIMENSION(_PATCH_NUM_CELLS,_FVM_Q_SIZE)    :: dQ
            real(kind = GRID_SR), DIMENSION(_FVM_CHUNK_SIZE,_FVM_Q_SIZE)            :: qL, qR, upd_qL, upd_qR
            real(kind = GRID_SR), DIMENSION(_FVM_CHUNK_SIZE,_FVM_AUX_SIZE)          :: auxL, auxR
            
            real(kind = GRID_SR), DIMENSION(_FVM_CHUNK_SIZE,2) :: normals_in_chunk
            integer, DIMENSION(_PATCH_NUM_CELLS) :: refinement_flags
            integer :: patch_refinement_flag
            integer, dimension(2) :: edge, cell_types
            real(kind=GRID_SR), dimension(2) :: normal
            real(kind = GRID_SR), dimension(2,3) :: vertices
            
            !DIR$ ASSUME_ALIGNED qL: 64
            !DIR$ ASSUME_ALIGNED qR: 64
            !DIR$ ASSUME_ALIGNED upd_qL: 64
            !DIR$ ASSUME_ALIGNED upd_qR: 64
            !DIR$ ASSUME_ALIGNED auxL: 64
            !DIR$ ASSUME_ALIGNED auxR: 64
            
            if (element%cell%geometry%i_plotter_type > 0) then ! if orientation = forward, reverse updates
                tmp=update1
                update1=update3
                update3=tmp
            end if
            
            ! init some variables
            refinement_flags = 0
            dQ = 0.0_GRID_SR
            maxWaveSpeed = 0.0_GRID_SR
            volume = cfg%scaling * cfg%scaling * element%cell%geometry%get_volume() / (_PATCH_NUM_CELLS)
            dt_div_volume = section%r_dt / volume
            edge_lengths = cfg%scaling * element%cell%geometry%get_edge_sizes() / _PATCH_BOUNDARY_SIZE
            
            associate(data => element%cell%data_pers)
            
                ! copy cell quantities to temporary arrays for left and right quantities
                do i=1, _PATCH_NUM_EDGES, _FVM_CHUNK_SIZE ! i -> init of chunk
                
                    edgesLeft = _PATCH_NUM_EDGES - i + 1 ! number of edges that still haven't been processed
                    numProblemsInChunk = min(edgesLeft, _FVM_CHUNK_SIZE)
                    
                    !upd_qL = 0.0_GRID_SR
                    !upd_qR = 0.0_GRID_SR
                    
                    do j=1, numProblemsInChunk ! j -> position inside chunk
                        ind = i + j - 1 ! actual index
                        
                        !get edge information from patch_geometry
                        call patch_geometry%get_edge(ind, int(element%cell%geometry%i_plotter_type),  edge, cell_types, normal)
                        
                        ! edge normals
                        normals_in_chunk(j,:) = normal

                        ! cells left to the edges
                        if (cell_types(1) == 0) then
                            ! data for internal cells come from cell data
                            qL  (j,:) = data%Q  (edge(1), :)
                            auxL(j,:) = data%AUX(edge(1), :)
                        else if (cell_types(1) == 1) then
                            ! data for ghost cells come from left edge (update) data
                            qL  (j,:) = update1%Q  (edge(1), :)
                            auxL(j,:) = update1%AUX(edge(1), :)
                        else if (cell_types(1) == 2) then
                            ! data for ghost cells come from bottom edge (update) data
                            qL  (j,:) = update2%Q  (edge(1), :)
                            auxL(j,:) = update2%AUX(edge(1), :)
                        else 
                            ! data for ghost cells come from right edge (update) date
                            qL  (j,:) = update3%Q (edge(1), :)
                            auxL(j,:) = update3%AUX(edge(1), :)
                        end if
                        
                        ! cells right to the edges
                        if (cell_types(2) == 0) then
                            ! data for internal cells come from cell data
                            qR  (j,:) = data%Q  (edge(2), :)
                            auxR(j,:) = data%AUX(edge(2), :)
                        else if (cell_types(2) == 1) then
                            ! data for ghost cells come from left edge (update) data
                            qR  (j,:) = update1%Q  (edge(2), :)
                            auxR(j,:) = update1%AUX(edge(2), :)
                        else if (cell_types(2) == 2) then
                            ! data for ghost cells come from bottom edge (update) data
                            qR  (j,:) = update2%Q  (edge(2), :)
                            auxR(j,:) = update2%AUX(edge(2), :)
                        else 
                            ! data for ghost cells come from right edge (update) date
                            qR  (j,:) = update3%Q (edge(2), :)
                            auxR(j,:) = update3%AUX(edge(2), :)
                        end if
                    end do


                    ! Now we compute the fluxes using either FVM_timestep_compute_fluxes_single_edge or FVM_timestep_compute_fluxes_multi_edge
                    ! (depending on whether _FVM_MULTI_EDGE was defined)
#                   if defined(_FVM_MULTI_EDGE)
                        ! *** CALL TO AN USER-DEFINED FUNCTION ***
                        call FVM_timestep_compute_fluxes_multi_edge(numProblemsInChunk, normals_in_chunk,qL,qR,auxL,auxR,upd_qL,upd_qR,maxWaveSpeedLocal)
                        
                        maxWaveSpeed = max(maxWaveSpeed, maxWaveSpeedLocal)

#                   else 
                    
#                       if defined(_PATCH_VEC_SIMD) || defined(_PATCH_VEC_INLINE)
                            ! Vectorization! (Requires OpenMP 4.0 or later)
                            !$OMP SIMD PRIVATE(maxWaveSpeedLocal) REDUCTION(max: maxWaveSpeed)
#                       endif
                        do j=1, numProblemsInChunk

#                           if defined(_PATCH_VEC_INLINE)
                                ! Warning: inlining this subroutine into an OMP SIMD loop may cause
                                ! bugs and incorrect calculations depending on the compiler version
                                ! Check the results!
                    
                                ! Recommended compiler: ifort 17.0
                    
                                !DIR$ FORCEINLINE
#                           endif

                            ! *** CALL TO AN USER-DEFINED FUNCTION ***
                            call FVM_timestep_compute_fluxes_single_edge(normals_in_chunk(j,:),qL(j,:),qR(j,:),auxL(j,:),auxR(j,:),upd_qL(j,:),upd_qR(j,:),maxWaveSpeedLocal)
                        
                            maxWaveSpeed = max(maxWaveSpeed, maxWaveSpeedLocal)
                        end do
#                   endif
                    
                    ! compute dQ
                    do j=1, numProblemsInChunk
                        ind = i + j - 1 ! actual index
                        
                        !get edge information from patch_geometry
                        call patch_geometry%get_edge(ind, int(element%cell%geometry%i_plotter_type),  edge, cell_types, normal)
                        
                        if (cell_types(1) == 0) then ! ignore ghost cells
                            dQ(edge(1),:) = dQ(edge(1),:) + upd_qL(j,:) * edge_lengths(patch_geometry%edge_types(ind))
                        end if
                        if (cell_types(2) == 0) then ! ignore ghost cells
                            dQ(edge(2),:) = dQ(edge(2),:) + upd_qR(j,:) * edge_lengths(patch_geometry%edge_types(ind))
                        end if
                    end do
                end do
                
                ! Update cells
#               if defined(_PATCH_VEC_SIMD) || defined(_PATCH_VEC_INLINE)
                ! Vectorization! (Requires OpenMP 4.0 or later)
                !--$OMP SIMD PRIVATE(vertices)
                ! TODO: make sure that this also gets vectorized
#               endif
                do i=1,_PATCH_NUM_CELLS
                
                    ! get real world coordinates for the three vertices
                    vertices = patch_geometry%get_vertices(i, int(element%cell%geometry%i_plotter_type))
                    do j=1,3
                        vertices(:,j) = samoa_barycentric_to_world_point(element%transform_data, vertices(:,j))
                        vertices(:,j) = vertices(:,j) * cfg%scaling + cfg%offset
                    end do                    
                
#                   if defined(_PATCH_VEC_INLINE)
                        !DIR$ FORCEINLINE
#                   endif
                    ! *** CALL TO AN USER-DEFINED FUNCTION ***
                    call FVM_timestep_update_cell(vertices, data%Q(i,:), data%AUX(i,:), dQ(i,:), section%r_dt, volume, refinement_flags(i))
                end do
                
                ! Handle refinement flag
                ! Logic: if there is at least one 1 in the patch -> refine (1)
                !        elseif there is at least one 0 -> keep (0)
                !        else coarsen (-1) (all cells must have been flagged as -1)
                patch_refinement_flag = maxval(refinement_flags(:)) 
                
                ! Check whether the refinement flag defined above is valid (compare with min and max depth)
                element%cell%geometry%refinement = 0
                if (element%cell%geometry%i_depth < cfg%i_max_depth .and. patch_refinement_flag == 1) then
                    element%cell%geometry%refinement = 1
                    traversal%i_refinements_issued = traversal%i_refinements_issued + 1_GRID_DI
                else if (element%cell%geometry%i_depth > cfg%i_min_depth .and. patch_refinement_flag == -1) then
                    element%cell%geometry%refinement = -1
                endif
               
                ! compute next dt
                section%r_dt_new = min(section%r_dt_new, volume / (edge_lengths(2) * maxWaveSpeed) )
            end associate

		end subroutine

		subroutine cell_last_touch_op(traversal, section, cell)
			type(t_fvm_euler_timestep_traversal), intent(inout)				:: traversal
			type(t_grid_section), intent(inout)							:: section
			type(t_cell_data_ptr), intent(inout)				:: cell
			real(kind = GRID_SR)							    :: b_norm

			b_norm = minval(abs(cell%data_pers%Q(:,1) - cell%data_pers%AUX(:,1))) !b_norm = minval(abs( h - b ))

			!refine also on the coasts
			if (cell%geometry%i_depth < cfg%i_max_depth .and. b_norm < 20.0_GRID_SR) then
				!cell%geometry%refinement = 1
				!traversal%i_refinements_issued = traversal%i_refinements_issued + 1_GRID_DI
			else if (b_norm < 100.0_GRID_SR) then
				!cell%geometry%refinement = max(cell%geometry%refinement, 0)
			endif
		end subroutine
		
        pure subroutine node_write_op(local_node, neighbor_node)
            type(t_node_data), intent(inout)			    :: local_node
            type(t_node_data), intent(in)				    :: neighbor_node

            !do nothing
        end subroutine


        pure subroutine edge_write_op(local_node, neighbor_node)
            type(t_edge_data), intent(inout)			    :: local_node
            type(t_edge_data), intent(in)				    :: neighbor_node

            !do nothing
        end subroutine
	END MODULE
#endif
