#if !defined(__FVM_MACROS_F90) && defined(_FVM)

#   define __FVM_MACROS_F90

    ! include the scenario-specific macros
#   if defined(_FVM_SWE)
#       include "FVM/SWE/FVM_SWE_macros.f90"
#   elif defined(_FVM_SWE2L)
#       include "FVM/SWE2L/FVM_SWE2L_macros.f90"
#   endif

#endif
