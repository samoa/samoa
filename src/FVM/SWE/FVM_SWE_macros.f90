#if !defined(__FVM_SWE_MACROS_F90)

#   define __FVM_SWE_MACROS_F90

#   if defined(_FVM_SWE)
        ! List of unknowns for SWE2L:
        ! 1 = h    (water height, top level)
        ! 2 = hu   (water momentum in x direction, top layer)
        ! 3 = hv   (water momentum in y direction, top layer)
#       define _FVM_Q_SIZE 3

        ! List of aux variables for SWE2L:
        ! 1 = b    (bathymetry)        
#       define _FVM_AUX_SIZE 1

        ! Custom chunk size:
#       define _FVM_CHUNK_SIZE 32

#   endif

#endif
