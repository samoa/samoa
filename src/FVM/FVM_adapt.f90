! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE


#include "Compilation_control.f90"

#if defined(_FVM)
	MODULE FVM_Adapt
		use SFC_edge_traversal
		use Conformity

		use Samoa
		use Tools_noise
		use FVM_euler_timestep
		
        use Tools_patch
        use FVM_module_wrapper

		implicit none

        type num_traversal_data
#           if _PATCH_DEPTH == 0
                real (kind=GRID_SR), dimension(_FVM_Q_SIZE)   :: Q
                real (kind=GRID_SR), dimension(_FVM_AUX_SIZE) :: AUX
#           endif
        end type

#		define _GT_NAME							t_fvm_adaption_traversal

#		define _GT_EDGES

#		define _GT_POST_TRAVERSAL_GRID_OP		post_traversal_grid_op
#		define _GT_PRE_TRAVERSAL_OP				pre_traversal_op
#		define _GT_POST_TRAVERSAL_OP			post_traversal_op

#		define _GT_TRANSFER_OP					transfer_op
#		define _GT_REFINE_OP					refine_op
#		define _GT_COARSEN_OP					coarsen_op

#		define _GT_CELL_TO_EDGE_OP				cell_to_edge_op

#		define _GT_NODE_MPI_TYPE

#		define _GT_NODE_WRITE_OP			    node_write_op
#		define _GT_EDGE_WRITE_OP			    edge_write_op

#		include "SFC_generic_adaptive_traversal.f90"

        subroutine create_node_mpi_type(mpi_node_type)
            integer, intent(out)            :: mpi_node_type

#           if defined(_MPI)
                type(t_node_data)                       :: node
                integer                                 :: blocklengths(2), types(2), disps(2), type_size, i_error
                integer (kind = MPI_ADDRESS_KIND)       :: lb, ub

                blocklengths(1) = 1
                blocklengths(2) = 1

                disps(1) = 0
                disps(2) = sizeof(node)

                types(1) = MPI_LB
                types(2) = MPI_UB

                call MPI_Type_struct(2, blocklengths, disps, types, mpi_node_type, i_error); assert_eq(i_error, 0)
                call MPI_Type_commit(mpi_node_type, i_error); assert_eq(i_error, 0)

                call MPI_Type_size(mpi_node_type, type_size, i_error); assert_eq(i_error, 0)
                call MPI_Type_get_extent(mpi_node_type, lb, ub, i_error); assert_eq(i_error, 0)

                assert_eq(0, lb)
                assert_eq(0, type_size)
                assert_eq(sizeof(node), ub)
#           endif
        end subroutine

		subroutine post_traversal_grid_op(traversal, grid)
			type(t_fvm_adaption_traversal), intent(inout)				:: traversal
			type(t_grid), intent(inout)							        :: grid
		end subroutine

		subroutine pre_traversal_op(traversal, section)
			type(t_fvm_adaption_traversal), intent(inout)				:: traversal
			type(t_grid_section), intent(inout)							:: section
		end subroutine

		subroutine post_traversal_op(traversal, section)
			type(t_fvm_adaption_traversal), intent(inout)				:: traversal
			type(t_grid_section), intent(inout)							:: section
		end subroutine
		!******************
		!Adaption operators
		!******************

		subroutine transfer_op(traversal, section, src_element, dest_element)
 			type(t_fvm_adaption_traversal), intent(inout)	                            :: traversal
			type(t_grid_section), intent(inout)											:: section
			type(t_traversal_element), intent(inout)									:: src_element
			type(t_traversal_element), intent(inout)									:: dest_element
            
            dest_element%cell%data_pers%Q   = src_element%cell%data_pers%Q
            dest_element%cell%data_pers%AUX = src_element%cell%data_pers%AUX
		end subroutine

		subroutine refine_op(traversal, section, src_element, dest_element, refinement_path)
 			type(t_fvm_adaption_traversal), intent(inout)	                            :: traversal
			type(t_grid_section), intent(inout)										    :: section
			type(t_traversal_element), intent(inout)									:: src_element
			type(t_traversal_element), intent(inout)									:: dest_element
			integer, dimension(:), intent(in)											:: refinement_path
			
            real (kind=GRID_SR), dimension(_PATCH_NUM_CELLS, _FVM_Q_SIZE)      :: Q_in, Q_out
            real (kind=GRID_SR), dimension(_PATCH_NUM_CELLS, _FVM_AUX_SIZE)    :: AUX_in, AUX_out
            real (kind = GRID_SR), DIMENSION(2,2,3)                                     :: vertices
            integer                                                                     :: patch_child_being_computed, child_patch_id, child_cell_ids(2)
			integer :: i, j, k, i_plotter_type, child
			
			Q_in   = src_element%cell%data_pers%Q
			AUX_in = src_element%cell%data_pers%AUX
            i_plotter_type = src_element%cell%geometry%i_plotter_type
            
            do i=1, size(refinement_path)
                ! compute 1st or 2nd child depending on orientation and refinement_path(i)
                ! and store it in output arrays, then store it input arrays for next refinement iteration.
                
                ! decide which patch child is being computed (first = left, second = right, consider a triangle with the right angle pointing upwards)
                if ( (refinement_path(i) == 1 .and. i_plotter_type>0) .or. (refinement_path(i) == 2 .and. i_plotter_type<0)) then
                    patch_child_being_computed = 1
                else
                    patch_child_being_computed = 2
                end if

#               if _PATCH_DEPTH > 0
                    ! the child patch values are always obtained by splitting the parent patch
                    do j=1, _PATCH_NUM_CELLS

                        ! get ids of children originated by refining cell j
                        call patch_geometry%get_child_ids(j, child_cell_ids, child_patch_id)
                    
                        ! Refine patch
                        ! This should only be done for half of the cells (the ones on this side of the patch)
                        if (patch_child_being_computed == child_patch_id) then
                        


                                ! get real world coordinates for the three vertices
                                do child=1,2
                                    vertices(child,:,:) = patch_geometry%get_vertices(child_cell_ids(child), int(dest_element%cell%geometry%i_plotter_type))
                                    do k=1,3
                                        vertices(child,:,k) = samoa_barycentric_to_world_point(dest_element%transform_data, vertices(child,:,k))
                                        vertices(child,:,k) = vertices(child,:,k) * cfg%scaling + cfg%offset
                                    end do
                                end do

                                ! *** CALL TO AN USER-DEFINED FUNCTION ***
                                call FVM_adapt_split_cell(Q_in(j,:), AUX_in(j,:), vertices(1,:,:), vertices(2,:,:),   &
                                            Q_out  (child_cell_ids(1),:), Q_out  (child_cell_ids(2),:), &
                                            AUX_out(child_cell_ids(1),:), AUX_out(child_cell_ids(2),:))
                        end if
                    end do
                    
#                   else

                        ! special case: _PATCH_DEPTH=0 -> _PATCH_NUM_CELLS=0
                        vertices(1,:,:) = patch_geometry%get_vertices(1, int(dest_element%cell%geometry%i_plotter_type))
                        do k=1,3
                            vertices(1,:,k) = samoa_barycentric_to_world_point(dest_element%transform_data, vertices(1,:,k))
                            vertices(1,:,k) = vertices(1,:,k) * cfg%scaling + cfg%offset
                        end do
                        
                        ! *** CALL TO AN USER-DEFINED FUNCTION ***
                        call FVM_adapt_split_cell(Q_in(1,:), AUX_in(1,:), vertices(1,:,:), vertices(1,:,:),   &
                                    Q_out  (1,:), Q_out  (1,:), &
                                    AUX_out(1,:), AUX_out(1,:))

#                   endif                    
                
                ! refined patches will have inverse orientation, so we need to invert this to compute the correct child
                i_plotter_type = -1 * i_plotter_type
                
                ! copy to input arrays for next iteration
                Q_in = Q_out
                AUX_in = AUX_out
                
            end do

            ! store results in dest_element
            dest_element%cell%data_pers%Q   = Q_out
            dest_element%cell%data_pers%AUX = AUX_out
            
		end subroutine

		subroutine coarsen_op(traversal, section, src_element, dest_element, refinement_path)
 			type(t_fvm_adaption_traversal), intent(inout)	                :: traversal
			type(t_grid_section), intent(inout)													:: section
			type(t_traversal_element), intent(inout)									:: src_element
			type(t_traversal_element), intent(inout)									:: dest_element
			integer, dimension(:), intent(in)											:: refinement_path
			
            integer                                                                     :: i, j
            real (kind = GRID_SR), DIMENSION(2,3)                                       :: vertices
            
            integer :: patch_child_given_in_input, child_cell_ids(2), child_patch_id

            ! decide which patch child was given in input (first = left, second = right, consider a triangle with the right angle pointing upwards)
            if ((refinement_path(1) == 1 .and. dest_element%cell%geometry%i_plotter_type>0) .or. (refinement_path(1) == 2 .and. dest_element%cell%geometry%i_plotter_type<0)) then
                patch_child_given_in_input = 1
            else
                patch_child_given_in_input = 2
            end if
            
            associate (src_data => src_element%cell%data_pers, dest_data => dest_element%cell%data_pers)
            
#               if _PATCH_DEPTH > 0
                    do i=1,_PATCH_NUM_CELLS ! iterate through cells of the new coarse patch being initialized
                        ! find out child ids
                        call patch_geometry%get_child_ids(i, child_cell_ids, child_patch_id)
                        
                        ! get real world coordinates for the the cell
                        vertices = patch_geometry%get_vertices(i, int(dest_element%cell%geometry%i_plotter_type))
                        do j=1,3
                            vertices(:,j) = samoa_barycentric_to_world_point(dest_element%transform_data, vertices(:,j))
                            vertices(:,j) = vertices(:,j) * cfg%scaling + cfg%offset
                        end do
                        
                        ! if this is the correct child patch, merge the child cells into parent's i cell
                        if (child_patch_id == patch_child_given_in_input)  then
                        
                            ! *** CALL TO AN USER-DEFINED FUNCTION ***
                            call FVM_adapt_merge_cells(src_data%  Q(child_cell_ids(1),:), src_data%  Q(child_cell_ids(2),:), &
                                                    src_data%AUX(child_cell_ids(1),:), src_data%AUX(child_cell_ids(2),:), &
                                                    vertices, dest_data%Q(i,:), dest_data%AUX(i,:))
                            
                        end if
                        
                    end do
#               else 
                    ! special case: _PATCH_DEPTH=0 -> _PATCH_NUM_CELLS=0
                    if (refinement_path(1) == 1) then
                        ! store data that will be used on next call, when refinement_path(1) == 2
                        traversal%Q   = src_data%Q(1,:)
                        traversal%AUX = src_data%AUX(1,:)
                    else
                        ! merge cells!
                        
                        ! get real world coordinates for the the cell
                        vertices = patch_geometry%get_vertices(1, int(dest_element%cell%geometry%i_plotter_type))
                        do j=1,3
                            vertices(:,j) = samoa_barycentric_to_world_point(dest_element%transform_data, vertices(:,j))
                            vertices(:,j) = vertices(:,j) * cfg%scaling + cfg%offset
                        end do
                        
                        ! *** CALL TO AN USER-DEFINED FUNCTION ***
                        call FVM_adapt_merge_cells(traversal%  Q(:), src_data%  Q(1,:), &
                                                   traversal%AUX(:), src_data%AUX(1,:), &
                                                    vertices, dest_data%Q(1,:), dest_data%AUX(1,:))
                    end if
                    
#               endif
            end associate

		end subroutine

        pure subroutine node_write_op(local_node, neighbor_node)
            type(t_node_data), intent(inout)			    :: local_node
            type(t_node_data), intent(in)				    :: neighbor_node

            !do nothing
        end subroutine


        pure subroutine edge_write_op(local_node, neighbor_node)
            type(t_edge_data), intent(inout)			    :: local_node
            type(t_edge_data), intent(in)				    :: neighbor_node

            !do nothing
        end subroutine
	END MODULE
#endif
