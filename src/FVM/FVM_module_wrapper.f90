! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE

#include "Compilation_control.f90"

#if defined(_FVM)

! Module used to switch between different systems of equations in this FVM scenario.
! Only one of the equation-specific modules will be "used", depending on the macros defined
!
! PS: At first I tried setting a macro directly to identify the used module name, e.g. #define _FVM_USED_MODULE FVM_SWE2L
! But this was sometimes too confusing for the compiler, and it couldn't find the respective .mod file... So I switched to this approach.
MODULE FVM_MODULE_WRAPPER
    
#   if defined(_FVM_SWE)
        use FVM_SWE
#   elif defined(_FVM_SWE2L)
        use FVM_SWE2L
#   endif

END MODULE

#endif