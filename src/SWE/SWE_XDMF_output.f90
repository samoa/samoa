#include "Compilation_control.f90"
#include "XDMF/XDMF_compilation_control.f90"

#if defined(_SWE)
    module SWE_XDMF_output
        
        use SWE_euler_timestep
        use Tools_openmp
        use Samoa_swe
        use Tools_patch
        use SFC_edge_traversal

        use XDMF_output_base
        use XDMF_xmf

        implicit none

        ! Parameter for the XDMF core API
        type(t_xdmf_parameter), parameter               :: param = t_xdmf_parameter( &
                                                            hdf5_valsg_width = 2, & ! 2 data fields: dimensions X and Y
                                                            hdf5_valst_width = 3) ! 3 entries per triangle

        ! For traversal data, use the XDMF core data structure as a base
        type num_traversal_data
            type(t_xdmf_base_output_traversal) :: base
        end type

#		define _GT_NAME								    t_swe_xdmf_output_traversal
#		define _GT_EDGES

#		define _GT_PRE_TRAVERSAL_OP					    pre_traversal_op
#		define _GT_POST_TRAVERSAL_OP				    post_traversal_op
#		define _GT_PRE_TRAVERSAL_GRID_OP				pre_traversal_grid_op
#		define _GT_POST_TRAVERSAL_GRID_OP				post_traversal_grid_op

#		define _GT_ELEMENT_OP						    element_op

#       define _GT_CELL_TO_EDGE_OP				        cell_to_edge_op

#		include "SFC_generic_traversal_ringbuffer.f90"

        ! This routine creates an array of wrappedsection pointers, 
        ! because fortran does not like arrays of pointers.
        subroutine ptr_wrap_sections(traversal, sections_ptr)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_xdmf_base_output_traversal_ptr), &
                dimension(:), allocatable, intent(out)                      :: sections_ptr

            integer                                                         :: error, i

            allocate(sections_ptr(size(traversal%sections)), stat = error); assert_eq(error, 0)
            do i = 1, size(traversal%sections)
                sections_ptr(i)%ptr => traversal%sections(i)%base
            end do
        end subroutine


        subroutine pre_traversal_grid_op(traversal, grid)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_grid), intent(inout)							            :: grid

            type(t_xdmf_base_output_traversal_ptr), &
                dimension(:), allocatable                                   :: sections_ptr
            integer                                                         :: error
            
            ! Pass this call the the core API
            call ptr_wrap_sections(traversal, sections_ptr)
            call xdmf_base_pre_traversal_grid_op(traversal%base, sections_ptr, grid, param)
            deallocate(sections_ptr, stat = error); assert_eq(error, 0)
        end subroutine

        subroutine post_traversal_grid_op(traversal, grid)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_grid), intent(inout)							            :: grid

            type(t_xdmf_base_output_traversal_ptr), &
                dimension(:), allocatable                                   :: sections_ptr
            integer                                                         :: error
            
            ! Pass this call the the core API
            call ptr_wrap_sections(traversal, sections_ptr)
            call xdmf_base_post_traversal_grid_op(traversal%base, sections_ptr, grid, param)
            deallocate(sections_ptr, stat = error); assert_eq(error, 0)

            if(rank_MPI .eq. 0) then
                ! Output the XMF file
                call write_xdmf(traversal%base, grid)
            end if
    
            traversal%base%output_iteration = traversal%base%output_iteration + 1
        end subroutine

        subroutine pre_traversal_op(traversal, section)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_grid_section), intent(inout)							    :: section

            traversal%base%sect_store_index = 1
        end subroutine

        subroutine post_traversal_op(traversal, section)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_grid_section), intent(inout)							    :: section

        end subroutine

        subroutine element_op(traversal, section, element)
            type(t_swe_xdmf_output_traversal), intent(inout)				:: traversal
            type(t_grid_section), intent(inout)							    :: section
            type(t_element_base), intent(inout)					            :: element

            real(REAL32), dimension(2, 3)			                        :: position
            integer(GRID_SI)	                                            :: i, offset_cells_buffer, offset_tree_buffer
            integer(INT64)                                                  :: element_hash
            real(REAL32)                                                    :: new_h, new_bigh
            integer(GRID_SI)                                                :: cell_offs
#           if _PATCH_DEPTH > 0
                integer(GRID_SI)                                            :: j, patch_cell_offs
                real(GRID_SR), dimension(2, 3)                              :: patch_cell_verts
#           endif

            ! Compute the cells cartesian position
            do i = 1, 3
                position(:, i) = real((cfg%scaling * element%nodes(i)%ptr%position) + cfg%offset(:), REAL32) 
            end do

            ! Get thread buffer offset
            offset_cells_buffer = traversal%base%root_layout_desc%ranks(rank_MPI + 1)%sections(section%index)%offset_cells_buffer
#           if _PATCH_DEPTH > 0
                offset_tree_buffer = offset_cells_buffer / _PATCH_NUM_CELLS
#           else
                offset_tree_buffer = offset_cells_buffer
#           endif

            ! Compute the cells offset in the topology tree
            call xdmf_hash_element(element, int(traversal%base%grid_scale, INT64), element_hash)

            ! Write the existence of this cell to the tree data buffer for this section
            if (element_hash .gt. (2_INT64**31 - 1)) then
                _log_write(1, *) " XDMF: Error: Cell hash does not fit into 32 bit signed integer. &
                    This is not yet implemented. This cell will be corrupt. Reduce level of detail / depth."
                assert(.false.)
                element_hash = 0
            end if
            traversal%base%sect_store%ptr%tree(traversal%base%sect_store_index + offset_tree_buffer) = int(element_hash, INT32)

#           if _PATCH_DEPTH > 0
                do i = 1, _PATCH_NUM_CELLS
                    ! Compute the global offset in the datasets of this cell
                    ! Note that the triangle order inside a patch is normalized here, i.e. independent from plotter direction
                    patch_cell_offs = ((traversal%base%sect_store_index - 1) * _PATCH_NUM_CELLS) + i
                    cell_offs = patch_cell_offs + offset_cells_buffer
                    ! Write integer attributes of this cell
                    traversal%base%sect_store%ptr%valsi(cell_offs, :) = (/ &
                        int(element%cell%geometry%i_depth, INT32), & ! Depth (refinement grade)
                        int(rank_MPI, INT32), & ! MPI rank
                        int(element%cell%geometry%i_plotter_type, INT32) /) ! Plotter type
                    ! For SWE, H stores the water height plus the bathymetry height
                    new_h = real(element%cell%data_pers%H(i) - element%cell%data_pers%B(i), REAL32)
                    if (new_h.le.cfg%dry_tolerance) then
                        new_h = 0
                    end if
                    new_bigh = real(element%cell%data_pers%H(i), REAL32)
                    ! Store the floating point attributes og this cell
                    traversal%base%sect_store%ptr%valsr(cell_offs, :) = (/ &
                        real(element%cell%data_pers%B(i), REAL32), & ! Bathymetry
                        new_h, new_bigh /)  ! Water height with and without bathymetry
                    ! Store the momentum vector for this cell
                    traversal%base%sect_store%ptr%valsr_uv(:, cell_offs) = (/ &
                        real(element%cell%data_pers%HU(i), REAL32), &
                        real(element%cell%data_pers%HV(i), REAL32) /)
                    ! Compute and store the actual position of this cell in the domain
                    patch_cell_verts = patch_geometry%get_vertices(i, int(element%cell%geometry%i_plotter_type, GRID_SI))
                    
                    forall(j = 1:param%hdf5_valst_width) traversal%base%sect_store%ptr%valsg(:, &
                        ((cell_offs - 1) * param%hdf5_valst_width) + j) = &
                        real(cfg%scaling * samoa_barycentric_to_world_point(element%transform_data, patch_cell_verts(:, j)) + cfg%offset, REAL32) !TODO
                end do
#           else
                cell_offs = traversal%base%sect_store_index + offset_cells_buffer
                ! Store cell values, see above for details
                traversal%base%sect_store%ptr%valsi(cell_offs, :) = (/ &
                    int(element%cell%geometry%i_depth, INT32), &
                    int(rank_MPI, INT32), &
                    int(element%cell%geometry%i_plotter_type, INT32) /)
                new_h = real(element%cell%data_pers%Q(1)%h - element%cell%data_pers%Q(1)%b, REAL32)
                if (new_h.le.cfg%dry_tolerance) then
                    new_h = 0
                end if
                new_bigh = real(element%cell%data_pers%Q(1)%h, REAL32)
                traversal%base%sect_store%ptr%valsr(cell_offs, :) = (/ &
                    real(element%cell%data_pers%Q%b, REAL32), &
                    new_h, new_bigh /)
                traversal%base%sect_store%ptr%valsr_uv(:, cell_offs) = (/ &
                    real(element%cell%data_pers%Q%p(1), REAL32), &
                    real(element%cell%data_pers%Q%p(2), REAL32) /)
                forall(i = 1:param%hdf5_valst_width) &
                    traversal%base%sect_store%ptr%valsg(:, &
                    ((cell_offs - 1) * param%hdf5_valst_width) + i) = position(:, i)
#           endif

            traversal%base%sect_store_index = traversal%base%sect_store_index + 1
        end subroutine

        ! This routine generates the XMF file needed to index the HDF5 files
        subroutine write_xdmf(base, grid)
            type(t_xdmf_base_output_traversal), intent(inout)				:: base
            type(t_grid), intent(inout)							            :: grid
    
            character(len = 256)					                        :: file_name_h5, file_name_xmf
            integer                                                         :: xml_file_id = 42
            integer(GRID_SI)                                                :: output_meta_iteration
            integer(HSIZE_T)                                                :: num_cells
            character(len = 17)                                             :: xml_time_string
            character(len = 21)                                             :: xml_dims_string
            character(len = 512)					                        :: xml_hdf5_path_string
            logical                                                         :: file_xmf_exist
            integer                                                         :: is_cp, i
    
            write(file_name_xmf, "(A, A, A)") trim(base%s_file_stamp), "_xdmf.xmf"
    
            inquire(file=trim(file_name_xmf)//char(0), exist=file_xmf_exist)
            if(file_xmf_exist) then
                ! If file exists, remove last line and write from there
                open(unit=xml_file_id, file=trim(file_name_xmf)//char(0), status="old", position="append", access="sequential", form="formatted")
                ! If needed, remove newer timesteps
                do i = 1, base%xdmf_remove_lines
                    backspace(xml_file_id)
                end do
            else
                ! If file does not exist, write XML header
                open(unit=xml_file_id, file=trim(file_name_xmf)//char(0), status="new", action="write", access="sequential", form="formatted")
                write(xml_file_id, "(A, A, A, A, A)", advance="yes") '<?xml version="1.0" encoding="UTF-8"?><Xdmf Version="3.0"><Domain>&
                    <Information Name="CommandLine" Value="', trim(cfg%xdmf%s_krakencommand), &
                    '" /><Information Name="FileStamp" Value="', trim(base%s_file_stamp), &
                    '" /><Grid GridType="Collection" CollectionType="Temporal">'
            end if
    
            output_meta_iteration = int(real(base%output_iteration, GRID_SR) / cfg%xdmf%i_xdmfspf, GRID_SI)
            write (file_name_h5, "(A, A, I0, A, A)") trim(base%s_file_stamp), "_", output_meta_iteration, "_xdmf.h5", char(0)
    
            write(xml_file_id, "(A)", advance="no") '<Grid>'

            ! Compute wether to output tree (checkpoint) data
            is_cp = 0
            if((cfg%xdmf%i_xdmfcpint.ne.0) .and. &
                mod(int(base%output_iteration, GRID_SI), int(cfg%xdmf%i_xdmfcpint, GRID_SI)).eq.0) then
                is_cp = 1
            end if
            write(xml_file_id, "(A, I0, A)", advance="no") '<Information Name="CP" Value="', is_cp, '" />'
    
            ! Time
            xml_time_string(:) = " "
            if (base%i_sim_iteration.eq.0) then
                write(xml_time_string, "(F0.8)") (-1.0_REAL32 / real(base%output_iteration + 1, REAL32))
            else
                write(xml_time_string, "(F0.8)") grid%r_time
            end if
            write(xml_file_id, "(A, A, A, I0, A, F0.8, A)", advance="no") '<Time Value="', trim(xml_time_string), '" />&
                <Attribute Name="Step" Center="Grid"><DataItem Format="XML" NumberType="Int" Dimensions="1">', &
                base%i_sim_iteration, '</DataItem></Attribute>&
                <Attribute Name="DeltaTime" Center="Grid"><DataItem Format="XML" NumberType="Float" Dimensions="1">', &
                grid%r_dt, '</DataItem></Attribute>'
    
            num_cells = int(base%num_cells, HSIZE_T)
#           if _PATCH_DEPTH > 0
                num_cells = num_cells * _PATCH_NUM_CELLS
#           endif
    
            ! Topology
            xml_dims_string(:) = " "
            write (xml_dims_string, "(I0, A, I0)") num_cells, " ", param%hdf5_valst_width
            xml_hdf5_path_string(:) = " "
            write (xml_hdf5_path_string, "(A, A, I0, A, I0, A, A)") trim(base%s_file_stamp_base), "_", &
                output_meta_iteration, "_xdmf.h5:/", base%output_iteration, "/", hdf5_valst_dname_nz
            write(xml_file_id, "(A, A, A, A, A)", advance="no") '<Topology TopologyType="Triangle"><DataItem Format="HDF" NumberType="Int" Dimensions="', &
                trim(xml_dims_string),'">', trim(xml_hdf5_path_string),'</DataItem></Topology>'
    
            ! Geometry
            xml_dims_string(:) = " "
            write (xml_dims_string, "(I0, A, I0)") (num_cells * param%hdf5_valst_width), " ", param%hdf5_valsg_width
            xml_hdf5_path_string(:) = " "
            write (xml_hdf5_path_string, "(A, A, I0, A, I0, A, A)") trim(base%s_file_stamp_base), "_", &
                output_meta_iteration, "_xdmf.h5:/", base%output_iteration, "/", hdf5_valsg_dname_nz
            write(xml_file_id, "(A, A, A, A, A)", advance="no") '<Geometry GeometryType="XY"><DataItem Format="HDF" NumberType="Float" Dimensions="', &
                trim(xml_dims_string),'">', trim(xml_hdf5_path_string),'</DataItem></Geometry>'
    
            ! Cell attributes
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_depth_dname_nz, "Depth", .true., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_rank_dname_nz, "Rank", .true., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_plotter_dname_nz, "Plotter", .true., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_b_dname_nz, "Bathymetry", .false., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_bh_dname_nz, "WaterHeight", .false., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 0_HSIZE_T, hdf5_attr_h_dname_nz, "WaterLevel", .false., xml_file_id)
            call xdmf_xmf_add_attribute(base%output_iteration, output_meta_iteration, base%s_file_stamp_base, &
                num_cells, 2_HSIZE_T, hdf5_attr_uv_dname_nz, "Momentum", .false., xml_file_id)
    
            write(xml_file_id, "(A)", advance="yes") '</Grid>'
    
            write(xml_file_id, "(A)", advance="no") '</Grid></Domain></Xdmf>'
            close(unit=xml_file_id)
        end subroutine

    end module
#endif
