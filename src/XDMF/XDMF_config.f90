#include "Compilation_control.f90"
#include "XDMF/XDMF_compilation_control.f90"

! This module provides routines to read metadata from XDMF files
module XDMF_config

    use Tools_log
    use Tools_openmp
    use Tools_mpi
    
    use XDMF_fox

    use, intrinsic :: iso_fortran_env

    implicit none

    ! This structure contains all XDMF-releated configuration variables
    type t_config_xdmf
        logical 				            :: l_xdmfoutput			                            !< xdmf output on/off
        character(256) 				        :: s_xdmfinput_file			                        !< xdmf input file
        integer                             :: i_xdmfcpint                                      !< write checkpoint data every n steps
        integer                             :: i_xdmfspf                                        !< amount of steps per file
        logical                             :: l_xdmfcheckpoint                                 !< xdmf is restarting from checkpoint
        logical                             :: l_xdmfcheckpoint_loaded                          !< cached data is loaded
        character(256) 				        :: s_xdmffile_stamp                                 !< cached output file stamp
        integer(INT32)                      :: i_xdmfoutput_iteration                           !< last output iteration
        integer(INT32)                      :: i_xdmfcheckpoint_iteration                       !< last checkpoint iteration
        integer(INT32)                      :: i_xdmfsimulation_iteration                       !< cached simulation iteration
        double precision                    :: r_xdmftime                                       !< cached simulation time
        double precision                    :: r_xdmfdeltatime                                  !< cached simulation time delta
        character(1024)                     :: s_krakencommand                                  !< cached command line for checkpointing
    end type

    contains

    ! This routine is meant to be called during initial configuration
    ! It reads the metadata of the last step found in the XDMF file
    subroutine xdmf_load_config(config, result)
        type(t_config_xdmf), intent(inout)  :: config
        integer, intent(out)                :: result

        character (1024)                    :: fox_attribute_text
        integer                             :: i, j, k
        logical                             :: xdmf_file_exists, is_cp
        type(t_fox_Node), pointer           :: fox_doc, fox_domain, fox_child, fox_attribute_name, fox_attribute_value
        type(t_fox_NodeList), pointer       :: fox_children, fox_grandchildren
        type(t_fox_NamedNodeMap), pointer   :: fox_attributes

        if(config%l_xdmfcheckpoint.and.(.not.config%l_xdmfcheckpoint_loaded)) then
            _log_write(0, ' (" XDMF: Loading checkpoint ", A)') trim(config%s_xdmfinput_file)
            config%l_xdmfcheckpoint_loaded = .true. ! Prevent recursion
            inquire(file=trim(config%s_xdmfinput_file)//char(0), exist=xdmf_file_exists)
            if(xdmf_file_exists) then
                fox_doc => fox_parseFile(trim(config%s_xdmfinput_file)//char(0))
                fox_children => fox_getElementsByTagName(fox_doc, "Domain")
                fox_domain => fox_item(fox_children, 0)

                fox_children => fox_getChildNodes(fox_domain)
                do i = 0, fox_getLength(fox_children) - 1
                    fox_child => fox_item(fox_children, i)

                    ! Read header information
                    if (fox_getLocalName(fox_child) == "Information") then
                        fox_attributes => fox_getAttributes(fox_child)
                        fox_attribute_name => fox_getNamedItem(fox_attributes, "Name")
                        fox_attribute_value => fox_getNamedItem(fox_attributes, "Value")
                        fox_attribute_text = fox_getNodeValue(fox_attribute_value)
                        select case(fox_getNodeValue(fox_attribute_name))
                            case("CommandLine")
                                _log_write(0, ' (" XDMF: Previous command line: ", A)') trim(fox_attribute_text)
                                config%s_krakencommand = trim(fox_attribute_text)
                            case("FileStamp")
                                _log_write(0, ' (" XDMF: Previous file stamp: ", A)') trim(fox_attribute_text)
                                config%s_xdmffile_stamp = trim(fox_attribute_text)
                        end select
                    else if (fox_getLocalName(fox_child) == "Grid") then
                        fox_children => fox_getElementsByTagName(fox_child, "Grid")
                        config%i_xdmfoutput_iteration = fox_getLength(fox_children) - 1

                        ! Search for last checkpoint
                        ol: do k = fox_getLength(fox_children) - 1, 1, -1
                            fox_child => fox_item(fox_children, k)
                            fox_grandchildren => fox_getChildNodes(fox_child)
                            do j = 0, fox_getLength(fox_grandchildren) - 1
                                fox_child => fox_item(fox_grandchildren, j)
                                if (fox_getLocalName(fox_child).eq."Information") then
                                    fox_attributes => fox_getAttributes(fox_child)
                                    fox_attribute_name => fox_getNamedItem(fox_attributes, "Name")
                                    if (fox_getNodeValue(fox_attribute_name).eq."CP") then
                                        call fox_extractDataAttribute(fox_child, "Value", is_cp)
                                        if (is_cp) then
                                            exit ol
                                        end if
                                    end if
                                end if
                            end do
                        end do ol

                        ! Select the last checkpoint
                        config%i_xdmfcheckpoint_iteration = k
                        if(config%i_xdmfcheckpoint_iteration.eq.0) then
                            _log_write(0, ' (" XDMF: ERROR: No last checkpoint step found ")')
                            call exit
                        end if
                        _log_write(0, ' (" XDMF: Previous last checkpoint step: ", I0)') config%i_xdmfcheckpoint_iteration
                        fox_child => fox_item(fox_children, config%i_xdmfcheckpoint_iteration)

                        ! Read the checkpoint
                        fox_children => fox_getChildNodes(fox_child)
                        do j = 0, fox_getLength(fox_children) - 1
                            fox_child => fox_item(fox_children, j)
                            ! Read information of the last step found
                            select case(fox_getLocalName(fox_child))
                                case ("Attribute")
                                    fox_attributes => fox_getAttributes(fox_child)
                                    fox_attribute_name => fox_getNamedItem(fox_attributes, "Name")
                                    fox_child => fox_getFirstChild(fox_child)
                                    select case(fox_getNodeValue(fox_attribute_name))
                                        case("Step")
                                            call fox_extractDataContent(fox_child, config%i_xdmfsimulation_iteration)
                                            _log_write(0, ' (" XDMF: Previous last simulation step: ", I0)') config%i_xdmfsimulation_iteration
                                        case("DeltaTime")
                                            call fox_extractDataContent(fox_child, config%r_xdmfdeltatime)
                                            _log_write(0, ' (" XDMF: Previous delta time: ", F0.8)') config%r_xdmfdeltatime
                                    end select
                                case ("Time")
                                    call fox_extractDataAttribute(fox_child, "Value", config%r_xdmftime)
                                    _log_write(0, ' (" XDMF: Previous last simulation time: ", F0.8)') config%r_xdmftime
                                    if (config%r_xdmftime.le.0) then
                                        _log_write(0, ' (" XDMF: Previous last simulation time is zero or lower. Restarting simulation from beginning.")')
                                        config%l_xdmfcheckpoint = .false.
                                    end if
                            end select
                        end do

                    end if
                end do

                call fox_destroy(fox_doc)
                result = 1
                return
            else
                _log_write(0, ' (" XDMF: Error: Checkpoint file not found ")')
                result = 0
                return
            end if
        end if
        result = -1
    end subroutine

#   if defined(_MPI)
        ! This routine scatters the config data read from a XDMF file to all other ranks
        subroutine xdmf_bcast_config(config)
            type(t_config_xdmf), intent(inout)  :: config

            integer                             :: error

            call mpi_bcast(config%s_krakencommand, 1024, MPI_CHARACTER, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%s_xdmffile_stamp, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%i_xdmfcheckpoint_iteration, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%i_xdmfoutput_iteration, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%i_xdmfsimulation_iteration, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%r_xdmftime, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            call mpi_bcast(config%r_xdmfdeltatime, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, error); assert_eq(error, 0)
            if(rank_MPI == 0) then
                _log_write(0, ' (" XDMF: Broadcasted and loading previous configuration options ")')
            end if
        end subroutine
#   endif

end module
