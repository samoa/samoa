#include "Compilation_control.f90"
#include "XDMF/XDMF_compilation_control.f90"

! This module provides common data types used by the XDMF routines
module XDMF_data_types

    use HDF5

    use, intrinsic :: iso_fortran_env

    implicit none

    ! Needed to prevent module conflicts
#   if defined(_SINGLE_PRECISION)
        integer, PARAMETER :: XDMF_GRID_SR = kind(1.0e0)
#   elif defined(_DOUBLE_PRECISION)
        integer, PARAMETER :: XDMF_GRID_SR = kind(1.0d0)
#   elif defined(_QUAD_PRECISION)
        integer, PARAMETER :: XDMF_GRID_SR = kind(1.0q0)
#    else
#       error "No floating point precision is chosen!"
#    endif

    integer, PARAMETER :: XDMF_GRID_SI = selected_int_kind(8)
    integer, PARAMETER :: XDMF_GRID_DI = selected_int_kind(16)

    integer, PARAMETER :: XDMF_SR = XDMF_GRID_SR
    integer, PARAMETER :: XDMF_SI = XDMF_GRID_SI
    integer, PARAMETER :: XDMF_DI = XDMF_GRID_DI

    ! Sizes of the datasets
    integer, parameter			         	:: hdf5_rank = 2
    integer(HSIZE_T), parameter				:: hdf5_tree_width = 2, hdf5_valsr_width = 3, hdf5_valsr_uv_width = 2, hdf5_valsi_width = 3
    integer(HSIZE_T), parameter				:: hdf5_sect_tree_width = 2
    integer(HSIZE_T), parameter, &
        dimension(hdf5_rank)			   	:: hdf5_tree_chunk_size = (/ hdf5_tree_width, 8_HSIZE_T /)
    integer(HSIZE_T), parameter             :: hdf5_vals_chunk_length = 32_HSIZE_T
    integer(HSIZE_T), parameter, &
        dimension(hdf5_rank)			   	:: hdf5_subset_stride = (/ 1, 1 /)
    integer(HSIZE_T), parameter, &
        dimension(hdf5_rank)			   	:: hdf5_subset_block = (/ 1, 1 /)

    ! Names of the datasets
    character(len = 1), parameter			:: hdf5_attr_dname_nz = "a"
    character(len = 2), parameter			:: hdf5_attr_dname = hdf5_attr_dname_nz//char(0)

    character(len = 2), parameter			:: hdf5_tree_dname = "t"//char(0)
    character(len = 2), parameter			:: hdf5_valsi_dname = "i"//char(0)
    character(len = 2), parameter			:: hdf5_valsr_dname = "r"//char(0)
    character(len = 1), parameter			:: hdf5_valsg_dname_nz = "g"
    character(len = 1), parameter			:: hdf5_valst_dname_nz = "p"
    character(len = 2), parameter			:: hdf5_valsg_dname = hdf5_valsg_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_valst_dname = hdf5_valst_dname_nz//char(0)

    character(len = 1), parameter			:: hdf5_attr_depth_dname_nz = "d"
    character(len = 1), parameter			:: hdf5_attr_rank_dname_nz = "o"
    character(len = 1), parameter			:: hdf5_attr_plotter_dname_nz = "l"
    character(len = 2), parameter			:: hdf5_attr_depth_dname = hdf5_attr_depth_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_rank_dname = hdf5_attr_rank_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_plotter_dname = hdf5_attr_plotter_dname_nz//char(0)
    character(len = 2), parameter, &
        dimension(hdf5_valsi_width)         :: hdf5_valsi_dnames = &
                                                  (/ hdf5_attr_depth_dname_nz, hdf5_attr_rank_dname_nz, hdf5_attr_plotter_dname_nz /)
    integer, parameter                      :: hdf5_valsi_plotter_offset = 3

    character(len = 1), parameter			:: hdf5_attr_b_dname_nz = "b"
    character(len = 1), parameter			:: hdf5_attr_h_dname_nz = "h"
    character(len = 1), parameter			:: hdf5_attr_bh_dname_nz = "k"
    character(len = 1), parameter			:: hdf5_attr_u_dname_nz = "u"
    character(len = 1), parameter			:: hdf5_attr_v_dname_nz = "v"
    character(len = 1), parameter			:: hdf5_attr_uv_dname_nz = "f"
    character(len = 2), parameter			:: hdf5_attr_b_dname = hdf5_attr_b_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_h_dname = hdf5_attr_h_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_bh_dname = hdf5_attr_bh_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_u_dname = hdf5_attr_u_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_v_dname = hdf5_attr_v_dname_nz//char(0)
    character(len = 2), parameter			:: hdf5_attr_uv_dname = hdf5_attr_uv_dname_nz//char(0)
    character(len = 2), parameter, &
        dimension(hdf5_valsr_width)         :: hdf5_valsr_dnames = &
                                                  (/ hdf5_attr_b_dname, hdf5_attr_h_dname, hdf5_attr_bh_dname /)
    integer, parameter                      :: hdf5_valsr_b_offset = 1, hdf5_valsr_bh_offset = 3

    ! This parameter controls the amount of data for a cell
    type t_xdmf_parameter
        integer(HSIZE_T)                    :: hdf5_valsg_width !< Amount of data associated with each data point
        integer(HSIZE_T)                    :: hdf5_valst_width !< Amount of data points per cell
    end type                                         

    ! This structure describes the location and space of a section in a HDF5 file
    type t_xdmf_section_descriptor
        integer(XDMF_GRID_SI)			    :: num_cells = 0, offset_cells = 0, offset_cells_buffer = 0
    end type

    ! This structure describes the layout of all sections of a rank in a HDF5 file
    type t_xdmf_section_descriptor_collection
        type(t_xdmf_section_descriptor), &
            dimension(:), allocatable       :: sections
        integer(XDMF_GRID_SI)               :: num_cells = 0, offset_cells = 0

        contains

        procedure, pass                     :: allocate => xdmf_file_descriptor_collection_allocate
        procedure, pass                     :: deallocate => xdmf_file_descriptor_collection_deallocate
    end type

    ! This structure describes the layout of all sections in a HDF5 file
    type t_xdmf_layout_descriptor
        type(t_xdmf_section_descriptor_collection), &
            dimension(:), allocatable       :: ranks

        contains

        procedure, pass                     :: allocate => xdmf_layout_descriptor_allocate
        procedure, pass                     :: deallocate => xdmf_layout_descriptor_deallocate
        procedure, pass                     :: scatter_to => xdmf_layout_descriptor_scatter_to
    end type

    contains

    subroutine xdmf_file_descriptor_collection_allocate(this, num_sections)
        class(t_xdmf_section_descriptor_collection), &
            intent(inout)                   :: this
        integer, intent(in)                 :: num_sections

        integer                             :: error

        allocate(this%sections(num_sections), stat = error); assert_eq(error, 0)
    end subroutine

    subroutine xdmf_file_descriptor_collection_deallocate(this)
        class(t_xdmf_section_descriptor_collection), &
            intent(inout)                    :: this

        integer                              :: error

        deallocate(this%sections, stat = error); assert_eq(error, 0)
    end subroutine

    subroutine xdmf_layout_descriptor_scatter_to(this, child)
        class(t_xdmf_layout_descriptor), &
            intent(inout)                    :: this
        class(t_xdmf_layout_descriptor), &
            intent(inout)                    :: child

        integer                              :: n, k, error

        allocate(child%ranks(size(this%ranks)), stat = error); assert_eq(error, 0)
        do n = 1, size(this%ranks)
            child%ranks(n)%num_cells = this%ranks(n)%num_cells
            child%ranks(n)%offset_cells = this%ranks(n)%offset_cells
            allocate(child%ranks(n)%sections(size(this%ranks(n)%sections)), stat = error); assert_eq(error, 0)
            do k = 1, size(this%ranks(n)%sections)
                child%ranks(n)%sections(k)%num_cells = this%ranks(n)%sections(k)%num_cells
                child%ranks(n)%sections(k)%offset_cells = this%ranks(n)%sections(k)%offset_cells
                child%ranks(n)%sections(k)%offset_cells_buffer = this%ranks(n)%sections(k)%offset_cells_buffer
            end do
        end do
    end subroutine

    subroutine xdmf_layout_descriptor_allocate(this, num_ranks)
        class(t_xdmf_layout_descriptor), &
            intent(inout)                   :: this
        integer, intent(in)                 :: num_ranks

        integer                             :: error

        allocate(this%ranks(num_ranks), stat = error); assert_eq(error, 0)
    end subroutine

    subroutine xdmf_layout_descriptor_deallocate(this)
        class(t_xdmf_layout_descriptor), &
            intent(inout)                   :: this

        integer                             :: n, error

        do n = 1, size(this%ranks)
            call this%ranks(n)%deallocate()
        end do
        deallocate(this%ranks, stat = error); assert_eq(error, 0)
    end subroutine

end module