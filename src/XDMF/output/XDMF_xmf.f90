#include "Compilation_control.f90"
#include "XDMF/XDMF_compilation_control.f90"

! This module provides routines to ease the generation of XDMF files
module XDMF_xmf

    use XDMF_data_types

    use, intrinsic :: iso_fortran_env

    implicit none

    contains

    ! This routine generates an XML tag describing a cell attribute in XDMF
    subroutine xdmf_xmf_add_attribute(output_iteration, output_meta_iteration, &
            file_stamp_base, dim_length, dim_width, attr_dname_nz, attr_cname, is_int, xml_file_id)
        integer (XDMF_GRID_SI), intent(in)					            :: output_iteration, output_meta_iteration
        character (len = *), intent(in)					                :: file_stamp_base
        integer (XDMF_GRID_DI), intent(in)					            :: dim_length, dim_width
        character (len = *), intent(in)					                :: attr_dname_nz, attr_cname
        logical, intent(in)                                             :: is_int
        integer, intent(inout)                                          :: xml_file_id

        character(len = 512)					                        :: xml_hdf5_path_string
        character(len = 21)                                             :: xml_dims_string
        character(len = 5)                                              :: xml_number_type_string

        xml_number_type_string = " "
        if(is_int) then
            write(xml_number_type_string, "(A)") "Int"
        else
            write(xml_number_type_string, "(A)") "Float"
        end if

        xml_dims_string(:) = " "
        if(dim_width.ne.0_HSIZE_T) then
            write (xml_dims_string, "(I0, A, I0)") dim_length, " ", dim_width
        else
            write (xml_dims_string, "(I0)") dim_length
        end if

        xml_hdf5_path_string(:) = " "
        write (xml_hdf5_path_string, "(A, A, I0, A, I0, A, A, A, A)") trim(file_stamp_base), &
            "_", output_meta_iteration, "_xdmf.h5:/", output_iteration, "/", hdf5_attr_dname_nz, "/", attr_dname_nz

        write(xml_file_id, "(A, A, A, A, A, A, A, A, A)", advance="no") '<Attribute Name="', attr_cname, '" Center="Cell"><DataItem Format="HDF" NumberType="', &
            trim(xml_number_type_string), '" Dimensions="', trim(xml_dims_string), '">', trim(xml_hdf5_path_string), '</DataItem></Attribute>'
    end subroutine

end module