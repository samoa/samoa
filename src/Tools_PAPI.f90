! Sam(oa)² - SFCs and Adaptive Meshes for Oceanic And Other Applications
! Copyright (C) 2010 Oliver Meister, Kaveh Rahnema
! This program is licensed under the GPL, for details see the file LICENSE

! Very basic PAPI wrapper. This could be extended for more features or improvements.

! For some reason unknown to me, PAPI is only working for one event - it fails when trying to add more events after the first one.
! Maybe it could work properly on other processors/computers, but at least on mine it doesn't.
! So some of the code below may assume only one event being measured at a time...
! (Although I've tried to make it as generic as possible to support multiple events)


#include "Compilation_control.f90"

module Tools_PAPI
#if defined(_PAPI)

    use Samoa

#   if defined(_OPENMP)
        use omp_lib
#   else
        use Tools_Log ! for omp_get_thread_num
#   endif    
    
    implicit none

#ifndef __PAPI_INCLUDED
#   define __PAPI_INCLUDED
#   include "f90papi.h"
#endif

#if !defined(_PAPI_EVENT)
#   define _PAPI_EVENT PAPI_DP_OPS
#endif


    integer, parameter              :: numevents = 1
    integer, dimension(numevents)   :: events = (/ _PAPI_EVENT /) ! usually: PAPI_L2_DCM, PAPI_DP_OPS 
    integer, allocatable            :: eventset(:)
    integer                         :: ret = PAPI_VER_CURRENT
    integer*8, dimension(numevents) :: counters
    
    contains
    
    subroutine papi_init()
        integer :: i
        integer :: tid, num_threads
        tid = omp_get_thread_num()+1
        num_threads = omp_get_num_threads()
        
        !$omp single
        allocate(eventset(num_threads))
        !$omp end single

        eventSet(tid) = PAPI_NULL
        
        ! Initialize PAPI library
        !$omp single
        call PAPIF_library_init(ret)
        if (ret .lt. 0) then
            print *, "[PAPI] FATAL: An error occured while initializing library!", ret
            call exit(1)
        end if
        !$omp end single

        ! Initialize current thread
        call PAPIF_thread_init(omp_get_thread_num, ret) 
        if (ret .lt. 0) then
            print *, "[PAPI] FATAL: An error occured while initializing thread!", ret
            call exit(1)
        end if
        
        counters = 0

        call PAPIF_create_eventset(eventset(tid), ret)
        ! Add events to be measured
        do i=1,numevents
            call PAPIF_add_event(eventset(tid), events(i), ret)
            if (ret .lt. PAPI_OK) then
                print *, "[PAPI] WARNING: Error when adding event. Event = ", i, "Error = ", ret
            end if
        end do

    end subroutine
    
    
    subroutine papi_start()
        integer :: tid
        tid = omp_get_thread_num()+1

        call PAPIF_start(eventset(tid), ret)
        if (ret .lt. PAPI_OK) then
            print *, "[PAPI] WARNING: Error when starting counters. Error = ", ret
        end if
    end subroutine

    subroutine papi_stop()
        integer*8, dimension(numevents) :: local_counters
        integer :: tid, i
        tid = omp_get_thread_num()+1
        
        call PAPIF_stop(eventset(tid), local_counters, ret)
        do i=1,numevents
            !$omp atomic
            counters(i) = counters(i) + local_counters(i)
        end do
        
        if (ret .lt. PAPI_OK) then
            print *, "[PAPI] WARNING: Error when stopping counters. Error = ", ret
        end if
    end subroutine
    
    function papi_get_counters() result (returned_counters)
        integer*8, dimension(numevents) :: returned_counters

        returned_counters = counters
    end function
    
    function papi_get_counter(ind) result(counter)
        integer, intent(in)   :: ind
        integer*8             :: counter
        
        counter = counters(ind)
    end function
    
    subroutine papi_stats()
        character(32) :: name
        integer :: i
        
        _log_write(0, *) ""
        _log_write(0, *) "PAPI counters: "
        do i=1,numevents
            call PAPIF_event_code_to_name(events(i), name, ret)
            _log_write(0, '(A, A, A, T34, I0)') " ", name, ": ", counters(i)
        end do
        _log_write(0, *) ""
        
    end subroutine

#endif
end module



