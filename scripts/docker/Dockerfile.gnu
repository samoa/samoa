FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin

# Package dependencies

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install git cmake build-essential autotools-dev autoconf g++ gfortran pkg-config scons mpich libmpich-dev

# XDMF and FoX

COPY scripts/XDMF /app/samoa/scripts/XDMF
WORKDIR /app/samoa/scripts/XDMF
RUN ./install_all.sh mpi gnu /app/samoa_xdmf_libs
RUN ./install_all.sh nompi gnu /app/samoa_xdmf_libs

# Debugging
RUN apt-get -y install gdb valgrind rsync

# Entrypoint

WORKDIR /app/samoa
CMD scons -j 8 config=my_conf_gnu.py