#!/bin/bash

FLAGS="--prefix=$3 --enable-fortran --enable-unsupported --enable-threadsafe --enable-optimization=high"
if [ "$1" = "mpi" ]; then
    FLAGS="$FLAGS --enable-parallel"
fi

mkdir -p libsrc
cd libsrc
git clone https://bitbucket.hdfgroup.org/scm/hdffv/hdf5.git
cd hdf5
git checkout hdf5-1_10_4
git reset --hard

echo "$3 HDF5: MPI: $1, Compiler: $2"
mkdir -p "build_$1_$2"
cd "build_$1_$2"
../configure $FLAGS && make -j 8 && make install