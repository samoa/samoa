[![Logo](https://raw.githubusercontent.com/meistero/Samoa/master/logo_small.png)]( https://raw.githubusercontent.com/meistero/Samoa/master/logo.png)

sam(oa)² 
=======

Space-Filling Curves and Adaptive Meshes for Oceanic And Other Applications.<br>
Gitlab repository: [https://gitlab.lrz.de/samoa/samoa ](https://gitlab.lrz.de/samoa/samoa)<br>
For further information on running sam(oa)² see our [Documentation] (https://samoa.readthedocs.io)<br>
Please contact Leonhard Rannabauer if you are interested in a close collaboration.<br><br>
[![Build Status](https://travis-ci.org/meistero/Samoa.svg)](https://travis-ci.org/meistero/Samoa)
