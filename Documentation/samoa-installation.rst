Installation
====================

Here we detail some general guidelines for installing sam(oa)² and its dependencies.
For installing sam(oa)²-flash, please visit :ref:`samoa-flash-installation`.


Dependency list
----------------

The following prerequisites are necessary for installing and running sam(oa)²:

- `git <http://git-scm.com/>`__
- `scons <http://www.scons.org/>`__
- gfortran 4.7 or higher OR Intel Fortran Compiler 13.0 or higher
- (Optional) `ASAGI <https://github.com/tum-i5/ASAGI>`__ v0.5.0 or higher for external geodata
- (Optional) Netcdf data files for ASAGI: For porous media flow, download the SPE10 data files from `SPE10 <http://www.spe.org/web/csp/datasets/set02.htm#download>`__. A script is included in the data directory that converts them to netcdf files. 
- (Optional) curl


Installing the dependencies
----------------------------

Here we detail some basic guidelines for installing the dependencies.

Linux system with admin privilege
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this case, packages can be straightforwardly installed using a package handling utility, e.g. for debian like systems:

.. code-block:: bash

     sudo apt-get install curl


HPC Cluster
^^^^^^^^^^^^

Typically, some libaries are preinstalled on HPC clusters through enviroment modules.
They can be loaded with the module command, e.g.:

.. code-block:: bash

     module load netcdf

It is also possible that some of the required environement variables are not properly set up by the module command. 
One way to look at what exactly the module command is initializing is to run:

.. code-block:: bash

     module display netcdf

For module not already installed on the HPC cluser, a manual installation is required. The same procedure is typically followed.
Libraries are installed by cloning a git repository, running cmake, and then make.

Once installed, some environement variables may need to be updated, for the system to be able to find the library.
$CPATH and $LIBRARY_PATH may have to be updated for a sucessful compilation and linking of samoa.
For example, if the dependency was installed in install-dir:

.. code-block:: bash

    export CPATH=install-dir/include/:$CPATH
    export LIBRARY_PATH=install-dir/lib:$LIBRARY_PATH

and $LD_LIBRARY_PATH should be updated for samoa to find dynamic libraries at run time:

.. code-block:: bash

    export LD_LIBRARY_PATH=install-dir/lib:$LD_LIBRARY_PATH
 
.. _Accessing-github-behind-a-firewall:

Accessing github behind a firewall
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some HPC servers restricts access to outside sources and thus does not allow connections to https servers.
Here we detail the case of the SuperMUC sever, but this can be adapted to other HPC servers.
There are two methods to clone sam(oa)² from github on the SuperMUC:

- By accessing the SuperMUC file system as a remote directory. git can then be executed locally:

.. code-block:: bash

        nohup sshfs <login>@supermuc.lrz.de:<samoa_dir> <local_dir>
        cd <local_dir>
        git clone https://gitlab.lrz.de/samoa/samoa .

- By login with remote port forwarding. In this case an alternative URL must be used to clone the git repository:

.. code-block:: bash

        ssh -X <login>@supermuc.lrz.de -R <port>:github.com:9418
        cd <samoa_dir>
        git clone git://localhost:<port>/meistero/Samoa .

This will download the source files for samoa into samoa_dir. 
This `page <https://seissol.readthedocs.io/en/latest/supermuc.html#accessing-github-from-supermuc>`__ provide further guidelines for setting a port forwarding.


Installing samoa
----------------

First clone samoa:

.. code-block:: bash

    git clone https://gitlab.lrz.de/samoa/samoa .

In order to view all the compilation options sam(oa)² provides, you can execute the following command now:

.. code-block:: bash

    cd samoa
    scons --help

Typical settings are:

.. code-block:: bash

    scons asagi_dir=<asagi_dir> compiler=gnu scenario=darcy -j<threads>
    scons asagi_dir=<asagi_dir> compiler=intel target=debug scenario=swe -j<threads>

If you wish to simulate simple scenarios that do not require data files you can also disable asagi with the flag

.. code-block:: bash

    scons asagi=No ...

Executables will be created in the bin directory.


SuperMUC-NG
----------------


Curl, ASAGI, ImpalaJIT and yaml-cpp have to be manually installed, following the procedure described above.
Once done, samoa can be build using:

.. code-block:: bash

   module load numactl/2.0.11-intel hdf5/1.8.20-intel-impi netcdf/4.6.1-intel-impi-hdf5v1.8-parallel
   export myLibs=path to installed libraries
   export CPATH=$myLibs/include/:$CPATH
   export LIBRARY_PATH=$myLibs/lib:$LIBRARY_PATH
   export LIBRARY_PATH=$NETCDF_BASE/lib/:$HDF5_BASE/lib/:$LIBRARY_PATH
   scons config=my_conf.py

Linux Cluster and MAC Cluster
------------------------------

The following modules should be loaded before compiling ASAGI and sam(oa)² on the Linux and MAC clusters

.. code-block:: bash

    module unload gcc python
    module load git cmake scons netcdf gcc/4.7
    module load gnuplot

sam(oa)² supports both multithreaded and single-threaded MPI. Both ASAGI and sam(oa)² must link to the same respective libraries, thus it is necessary to compile ASAGI twice:
once without MT support and once with MT support. Rename the single-threaded library to "libasagi_nomt.so" and the multi-threaded library to "libasagi.so".

At this point, you should be able to compile ASAGI and sam(oa)².


