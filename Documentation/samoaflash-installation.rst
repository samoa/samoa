.. _samoa-flash-installation:

sam(oa)²-flash installation
===========================

sam(oa)²-flash comes in pair with a set of ``bash`` shell installation scripts, which automatize its installation.
The scripts are able to download and install the ``hdf5``, ``fox``, ``netcdf_c``, ``netcdf_cpp`` and ``asagi`` dependencies.

First clone samoa and checkout the flash-testing-xdmf branch:

.. code-block:: bash

    git clone https://gitlab.lrz.de/samoa/samoa .
    git checkout flash-testing-xdmf

The installation scripts are to be found in ``script/XDMF``.

Prerequisites
----------------

**Important!** The default and recommended behavior is to install all libraries into a directory outside of the default system search path. This is to prevent library conflicts with the existing system. However, if you already have a version of HDF5 and/or FoX, ASAGI or NetCDF in your search path (either by installing from the distribution package sources or by loading a module), this might cause trouble. Check your link path and order carefully.

The scripts require ``git``, ``autotools`` and ``cmake`` in addition to the GNU compiler or intel compiler toolchain.

Scripts configuration
---------------------

The scripts can be run in 4 modes of configuration:

 - GNU compiler without MPI ``gnu nompi``
 - GNU compiler with MPI ``gnu mpi``
 - Intel compiler without MPI ``intel nompi``
 - Intel compiler with MPI ``intel mpi``

The following environment variables may be configured:

 - ``GIT_PROTOCOL``. May be either ``https`` (default) or ``ssh``. Use this if you have to tunnel your outgoing internet connections (e.g. on SuperMUC-NG, see :ref:`Accessing-github-behind-a-firewall`).
 - ``ASAGI_NUMA``. Controls ASAGI NUMA support. Default: ``-DNONUMA=1`` (disabled NUMA). Set to ``-DNONUMA=0`` to enable NUMA support.
 - ``HDF5_URL``. May be used to override the HDF5 git repository URL. Use this if you have no SSH access to the original HDF5 BitBucket repository.

Installation scripts
---------------------

- ``./install_all_hpc.sh [library directory path]``: Install all required libraries into the specified directory, using the intel compiler with MPI support. If no directory is specified, the default location ``~/local`` will be used. It is advised to provide an absolute path, however testing whether this is strictly necessary is yet inconclusive. Use this script to setup your HPC environment.
- ``./install_all.sh <mpi|nompi> <intel|gnu> [library directory path]``: This script is called by the ``./install_all_hpc.sh`` script with the parameters ``mpi intel``. It can be used to specify the alternative GNU compiler or to specify the MPI support e.g. for testing.
- ``./install_lib.sh <name> <mpi|nompi> <intel|gnu> [library directory path]``: This script is repeatedly called by the ``./install_all.sh`` script. It downloads and installs a specific library in the given configuration.
- ``./asagi.sh``, ``./fox.sh``, ``./hdf5.sh``, ``./netcdf_c.sh``, ``./netcdf_cxx.sh``: These scripts are not meant to be called directly. Instead, use on of the wrapper scripts above. These scripts contain download and build instructions for the specific libraries. You may edit these in order to adjust the download sources or compilation flags.

After a successful compilation, the specified directory will contain the required libraries in ``<directory>/<compiler>/<serial|parallel>/lib`` and the header files in ``<directory>/<compiler>/<serial|parallel>/include``. Feed this to your linker. For example, if you were to install the libraries to ``/opt/samoa_xdmf_libs`` using the intel compiler with MPI support, then the libraries would be located at ``/opt/samoa_xdmf_libs/intel/parallel/lib``.
Further information are available at `this link <https://gitlab.lrz.de/samoa/samoa/-/blob/flash-testing-xdmf/scripts/XDMF/README.md>`__.


Custom built vs. pre-installed packages
-------------------------------------------

Some Linux distributions do not yet package the recent required versions of HDF5 and FoX. In such cases the compilation from source is recommended.

Using the the pre-installed modules on CoolMUC and SuperMUC-NG is possible. The recommended modules are:
- hdf5/1.10.2-intel-impi-threadsafe

For ASAGI:
- netcdf/4.6.1-intel-impi-hdf5v1.10-parallel
- netcdf-cxx4/4.3.0-intel-impi-hdf5v1.10
- netcdf-fortran/4.4.4-intel-impi-hdf5v1.10

Examples
---------------------

- Local testing, using GCC and no MPI: 

.. code-block:: bash

    ./install_all.sh nompi gnu /opt/samoa_xdmf_libs

- Setup on SuperMUC-NG:

.. code-block:: bash

    GIT_PROTOCOL=ssh ASAGI_NUMA='-DNONUMA=0' HDF5_URL='ssh://gitlab.lrz.de/ChristophHonal/hdf5.git' ./install_all_hpc.sh /dss/dsshome1/0D/ga63yos3/samoa_xdmf_libs


This command requires a working SSH tunnel to GitHub and LRZ GitLab (see :ref:`Accessing-github-behind-a-firewall`).

This command overrides the HDF5 repository URL, because the author has no SSH access to the HDF5 bitbucket, and SuperMUC-NG requires all outgoing connections to be via a SSH tunnel. The URL given is accessible to anyone logged in into LRZ GitLab.

This command installs the libraries to ``/dss/dsshome1/0D/ga63yos3/samoa_xdmf_libs``, which is in the home directory of the author. Please adjust this to your own.

sam(oa)²-flash installation
-----------------------------
Once the dependencies have been installed, sam(oa)²-flash can be installed with scons.
In order to view all the compilation options sam(oa)² provides, you can execute the following command now:

.. code-block:: bash

    scons --help

The compilation can be configurated either inline, e.g.:

.. code-block:: bash

    scons asagi_dir=<asagi_dir> compiler=gnu scenario=asagi -j<threads>

or through a configuration python file (here conf.py):

.. code-block:: bash

    scons config=conf.py


Provided you are using a branch with proper XDMF support, the Samoa SCons configuration takes the following arguments, for example if the libraries were installed to ``/opt/samoa_xdmf_libs``, using the GNU compiler with MPI support:

.. code-block:: bash

    xdmf='true'
    xdmf_fox_dir='/opt/samoa_xdmf_libs/gnu/parallel'
    xdmf_hdf5_dir='/opt/samoa_xdmf_libs/gnu/parallel'

When using ASAGI (`asagi='true'`), you must also configure the ASAGI and NetCDF linker paths:

.. code-block:: bash

    asagi_dir='/opt/samoa_xdmf_libs/gnu/parallel'
    netcdf_dir='/opt/samoa_xdmf_libs/gnu/parallel'



