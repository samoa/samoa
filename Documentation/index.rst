.. Samoa documentation master file

============
User Manual
============

sam(oa)² (https://gitlab.lrz.de/samoa/samoa) is a software package for a dynamically adaptive, parallel solution of 2D partial differential equations on triangular grids.
Application include multiphase flow in heterogeneous porous media and tsunami wave propagation (see sam(oa)²-flash).


.. toctree::
  :maxdepth: 3
  :caption: sam(oa)²

  samoa-introduction
  samoa-installation


.. toctree::
  :maxdepth: 3
  :caption: sam(oa)²-flash

  samoaflash-introduction
  converting-SeisSol-output
  samoaflash-installation

.. toctree::
  :maxdepth: 3
  :caption: running sam(oa)²

  running-samoa
  samoaflash-parameters

