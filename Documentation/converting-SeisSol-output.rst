Converting SeisSol output files
================================

`SeisSol <https://github.com/SeisSol/SeisSol>`__ is a scientific software for the numerical simulation of seismic wave phenomena and earthquake dynamics.
We here detail how to convert output files from SeisSol to sam(oa)²-flash compatible files.  

sam(oa)²-flash reads the ground displacement data in a netcdf file format.
SeisSol outputs the ground surface displacement time-histories as an unstructured grid (surface output), and support several file types (Hdf5, posix) and data format (float or double).  


To convert such output data to a netcdf file, the `displacement-converter <https://gitlab.lrz.de/samoa/displacement-converter>`__ can be used.
The procedure to compile the ``displacement-converter`` is detailed in the README file.
The ``displacement-converter`` is fully functional, but some refactoring may be required to merge functionalities on different branches.  

The ``master`` branch allows simply converting a SeisSol surface output to netcdf. It only supports Hdf5 files, in double precision.  

The ``thomas/tanioka_reconstruction`` adds the contribution of the horizontal displacements (Tanioka and Satake, 1996) to the vertical displacements. 
This branch support any possible output file from SeisSol.
The ``--fault_x1 --fault_x2 --fault_y1 --fault_y2`` options allow defining a rectangular area over which the earthquake displacements are not interpolated.
Else, the discontinuity from surface faulting may be smoothed out.


Spatio-temporal extent of the displacement file
-------------------------------------------------

The spatial extent of the displacement file (and of the earthquake scenario) should be large enough to capture the coseismic displacement area.
Also, the simulated duration of the earthquake scenario has to be long enough to ensure that all significant seismic waves, in particular, the surface waves, have left the domain.
Else, such transient waves may translate into unphysical permanent surface offsets at the end of the surface displacement phase.
