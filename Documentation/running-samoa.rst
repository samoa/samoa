
Parallel run configuration
===========================

sam(oa)² provides parallelization in shared (using OpenMP) and distributed (via MPI) memory.
The number of thread (openMP) is to be set through the environement variable ``OMP_NUM_THREADS``.

.. code-block:: bash

    export OMP_NUM_THREADS=nthreads

The number of rank is then set with mpirun when running the code:

.. code-block:: bash

    mpirun -n nranks ./samoa_flash options

This leads to sam(oa)² being executed on nthreads*nranks cpus.


Execution
====================

For execution parameters refer to the online help by calling the executable with '-h' or '--help'.

