Introduction
====================

sam(oa)²-flash models tsunami propagation and inundation by solving the depth-integrated (hydrostatic) shallow water equations using adaptive mesh refinement on tree-structured triangular grids.
It implements the limiter-based well-balanced second-order Runge-Kutta discontinuous Galerkin method developed by Vater et al. (2019), allowing wave propagation with high accuracy.
The scheme is mass-conservative, preserves positivity of the fluid depth and accurately computes small perturbations from the still water state at rest (e.g., tsunami waves). 
The influence of bathymetry and bottom friction is parameterized through source terms. 
In particular, bottom friction is parameterized through Manning friction by a split-implicit discretization.
It features an accurate and robust wetting and drying scheme for the simulation of flooding and drying events at the coast (Vater and Behrens (2014), Vater et al. (2015, 2019)).
Adaptive mesh refinement allows for  efficiently tsunami modeling over large domains, while simulatenously allowing for high local resolution.


References
==============

Vater, S., & Behrens, J. (2014). Well-balanced inundation modeling for shallow-water flows with discontinuous Galerkin schemes. In Finite volumes for complex applications VII-elliptic, parabolic and hyperbolic problems (pp. 965-973). Springer, Cham.

Vater, S., Beisiegel, N., & Behrens, J. (2015). A limiter-based well-balanced discontinuous Galerkin method for shallow-water flows with wetting and drying: One-dimensional case. Advances in water resources, 85, 1-13.

Vater, S., Beisiegel, N., & Behrens, J. (2019). A limiter‐based well‐balanced discontinuous Galerkin method for shallow‐water flows with wetting and drying: Triangular grids. International Journal for Numerical Methods in Fluids, 91(8), 395-418
