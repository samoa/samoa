Introduction
====================

sam(oa)² (https://gitlab.lrz.de/samoa/samoa) is a software package for a dynamically adaptive, parallel solution of 2D partial differential equations on triangular grids.
It implements a memory efficient algorithm for grid generation, refinement, and traversal, base on space-filling curves.
sam(oa)² features efficient adaptive mesh refinement for tree-structured triangular meshes and provides parallelization in shared (using OpenMP) and distributed (via MPI) memory.
It has been shown to scale up to thousands of compute cores, with problem sizes that exceed one billion grid cells with dynamic adaptive refinement and coarsening of cells (Meister et al., 2016).
Application include multiphase flow in heterogeneous porous media and tsunami wave propagation (see SamoaFlash).

Related publication
====================

Meister, O., Rahnema, K., & Bader, M. (2016). Parallel memory-efficient adaptive mesh refinement on structured triangular meshes with billions of grid cells. ACM Transactions on Mathematical Software (TOMS), 43(3), 1-27, URL, http://delivery.acm.org/10.1145/2950000/2947668/a19-meister.pdf.
