sam(oa)²-flash parameters
================================

Properly resolving islands
--------------------------


Upon initialization, sam(oa)² starts with a minimum grid depth (``d = dmin``).
The coast cells are refined to the maximum grid depth.
A cell is considered coast cell if ``abs(bathymetry) < refined_bathymetry`` for one of its vertex.
Tiny islands may not be properly captured by the vertices of the initial grid if ``dmin`` is not large enough.
In this case, upon refinement (e.g. when the first waves reach the island), such islands would be suddenly resolved.
This sudden spurious offset of the bathymetry will source some unphysical waves.


displacement_height 
------------------------------

During the earthquake phase, a stepwise refinement strategy is enforced.
Large displacements are highly refined, while small displacements are less refined.
The ``displacement_height`` parameter offers a reference value for evaluating the magnitude of the displacement.
If set smaller than the actual maximum displacement, a larger region of the displacement will be refined.


dry_tolerance
------------------------------


``dry_tolerance`` is a threshold below which cells are interpreted as dry and not considered in the timestep computation anymore.
Decreasing ``dry_tolerance`` allows more accurate results at the coast (inundation), but leads to smaller time step size and then longer time to solution.
Note also that a too low value of ``dry_tolerance`` can lead to stability issue:
In shallow water equations, the eigenvalues are ``p/h + sqrt(gh)`` and ``p/h - sqrt(gh)``.
The dry case is ``h->0``.
In the analytic case, when ``h->0``, so does the momentum and ``p/h`` is constant.
In the numeric case ``h->0`` does not necessarily come in pair with ``p->0``, and ``p/h`` can diverge.
The dry tolerance gives us a maximal eigenvalue ``p/dry_tolerance``.
In short, ``dry_tolerance`` constrains the trade-off between accuracy, stability, and simulation time.
